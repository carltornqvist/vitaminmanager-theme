<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package IndusPress
 */

if ( ! have_posts() )
{
	get_template_part( '404' );
	return;
}

// Trigger the_post() to setup post data. The data like author info will be used in hero area.
the_post();
?>

<?php get_header(); ?>
<div class="container">
	<section id="content" class="content">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-header">
				<?php
				/**
				 * Display social sharing buttons for single posts.
				 * For developers: you can remove these social buttons (if you want to use another plugin) by using the code:
				 * remove_theme_support( 'induspress-social-buttons' )
				 */
				if ( current_theme_supports( 'induspress-social-buttons' ) )
				{
					require_once get_template_directory() . '/inc/social-buttons.php';
					$social_buttons = new IndusPress_Social_Buttons;
					$social_buttons->render();
				}
				?>
			</div>
			<!-- .entry-header -->
			<div class="entry-content clearfix">
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			</div>

			<footer class="entry-footer">
				<?php
				// Post tags
				the_tags( '<p class="post-tags">', '', '</p>' );

				// Previous/next post navigation.
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'induspress' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'induspress' ) . '</span> ' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'induspress' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'induspress' ) . '</span> ' .
						'<span class="post-title">%title</span>',
				) );
				?>
			</footer>

			<?php
			if ( comments_open() || get_comments_number() )
			{
				comments_template( '', true );
			}
			?>

		</article>

	</section>

	<?php get_sidebar();?>
</div>
<?php get_footer(); ?>
