<?php
/**
 * Template Name: Front Page
 * Description: Display content for the front page.
 * @package IndusPress
 */

get_header(); ?>

<section id="content" class="section content">
	<div class="container clearfix">
		<?php if ( have_posts() ): the_post(); ?>
			<?php the_content(); ?>
		<?php endif; ?>
	</div>
</section>

<?php dynamic_sidebar( 'home' ); ?>
<?php get_footer(); ?>
