<?php
/**
 * Template Name: Download
 * Description: Display page content download .
 * @package IndusPress
 */

get_header(); ?>

<div class="container">
	<section id="content" class="content">
		<section class="section downloads">
			<div class="grid">
				<div class="column two-third">
					<?php if ( have_posts() ): the_post(); ?>

						<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php endif; ?>
				</div>
				<div class="column one-third">
					<div class="accordion">
						<?php
						if ( is_active_sidebar( 'download' ) )
						{
							dynamic_sidebar( 'download' );
						}
						?>
					</div>
				</div>
			</div>
		</section>
	</section>
</div>

<?php get_footer(); ?>
