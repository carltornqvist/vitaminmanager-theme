<?php
/**
 * Template Name: Wide content
 * Description: Display page content widely.
 * @package IndusPress
 */

get_header(); ?>

<div class="container">
	<section id="content" class="content">

		<?php if ( have_posts() ): the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php
			if ( comments_open() || get_comments_number() )
			{
				comments_template( '', true );
			}
			?>

		<?php endif; ?>

	</section>
</div>

<?php get_footer(); ?>
