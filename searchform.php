<?php
/**
 * The template part for displaying search form.
 * @package IndusPress
 */
?>
<form method="get" class="searchform" action="<?php echo esc_url( home_url() ); ?>" role="search">
	<label>
		<span class="screen-reader-text"><?php _ex( 'Search for:', 'label', 'induspress' ); ?></span>
		<input type="search" class="addsearch search-field" name="s" id="s" placeholder="<?php _e( 'Search form', 'induspress' ); ?>" value="<?php echo trim( get_search_query() ); ?>">
	</label>
	<button type="submit" id="searchsubmit" class="search-button">
		<i class="icon icon-magnifying-glass"></i><span class="screen-reader-text"><?php _e( 'Search', 'induspress' ); ?></span>
	</button>
</form>
