<?php
/**
 * Register theme options in the Customizer
 * @package IndusPress
 */

/**
 * Register theme options in the Customizer
 * @package IndusPress
 */
class IndusPress_Customizer
{
	/**
	 * Add hooks for customizer
	 */
	public function __construct()
	{
		add_action( 'customize_register', array( $this, 'register' ) );
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	/**
	 * Register customizer settings
	 * @param WP_Customize_Manager $wp_customize WordPress customizer manager instance
	 */
	public function register( WP_Customize_Manager $wp_customize )
	{
		require_once get_template_directory() . '/inc/customizer/sanitizer.php';
		$sanitizer = new IndusPress_Customize_Sanitizer;

		// Theme option panel
		$wp_customize->add_panel( 'induspress', array(
			'title' => __( 'Theme Options', 'induspress' ),
		) );

		// Theme option sections
		$wp_customize->add_section( 'header', array(
			'title' => __( 'Header', 'induspress' ),
			'panel' => 'induspress',
		) );

		// Footer option
		$wp_customize->add_section( 'footer', array(
			'title' => __( 'Footer', 'induspress' ),
			'panel' => 'induspress',
		) );

		// Logo
		$wp_customize->add_setting( 'logo', array(
			'sanitize_callback' => array( $sanitizer, 'image' ),
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control(
			$wp_customize,
			'logo',
			array(
				'label'       => __( 'Logo', 'induspress' ),
				'section'     => 'header',
				'settings'    => 'logo',
				'description' => __( 'The logo will be displayed on the left of the header. If no logo is selected, the website title will be displayed.', 'induspress' ),
			)
		) );

		// Display site title and tagline
		$wp_customize->add_setting( 'display_site_title', array(
			'sanitize_callback' => array( $sanitizer, 'checkbox' ),
		) );
		$wp_customize->add_control( 'display_site_title', array(
			'label'   => __( 'Display site title and tagline?', 'induspress' ),
			'section' => 'header',
			'type'    => 'checkbox',
		) );

		// Header top text
		$wp_customize->add_setting( 'header_top_text', array(
			'sanitize_callback' => array( $sanitizer, 'html' ),
		) );
		$wp_customize->add_control( 'header_top_text', array(
			'label'       => __( 'Header top text', 'induspress' ),
			'section'     => 'header',
			'settings'    => 'header_top_text',
			'type'        => 'textarea',
			'description' => __( 'This text will be displayed above the logo and below the topbar, on the right of the header area. HTML is allowed.', 'induspress' ),
		) );

		// Footer text
		$wp_customize->add_setting( 'footer_left', array(
			'sanitize_callback' => array( $sanitizer, 'html' ),
		) );
		$wp_customize->add_control( 'footer_left', array(
			'label'       => __( 'Footer left text', 'induspress' ),
			'section'     => 'footer',
			'settings'    => 'footer_left',
			'type'        => 'textarea',
			'description' => __( 'This text will be displayed on the left of the footer. HTML and shortcode are allowed.', 'induspress' ),
		) );
		$wp_customize->add_setting( 'footer_right', array(
			'sanitize_callback' => array( $sanitizer, 'html' ),
		) );
		$wp_customize->add_control( 'footer_right', array(
			'label'       => __( 'Footer right text', 'induspress' ),
			'section'     => 'footer',
			'settings'    => 'footer_right',
			'type'        => 'textarea',
			'description' => __( 'This text will be displayed on the right of the footer. HTML and shortcode are allowed.', 'induspress' ),
		) );
	}

	/**
	 * Enqueue script for customizer control
	 */
	public function enqueue()
	{
		wp_enqueue_script( 'induspress-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'jquery' ), '', true );
		wp_localize_script( 'induspress-customizer', 'indusPressCustomizer',
			array(
				'docs' => esc_html__( 'View documentation', 'induspress' ),
			)
		);
	}
}
