<?php
/**
 * Products widget: Displays latest posts on the homepage.
 * @package IndusPress
 */

/**
 * Products widget class.
 * @package IndusPress
 */
class IndusPress_Widget_News extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'    => '',
			'subtitle' => '',
		);
		parent::__construct( 'induspress_news', __( 'IndusPress: News', 'induspress' ), array(
			'classname'   => 'news',
			'description' => __( 'Displays latest posts on the homepage.', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );

		$query = new WP_Query( array(
			'posts_per_page'      => 6,
			'ignore_sticky_posts' => true,
		) );
		if ( ! $query->have_posts() )
		{
			return;
		}

		echo $args['before_widget'];
		?>
		<div class="container clearfix">
			<?php
			if ( $instance['title'] )
			{
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
			}
			if ( $instance['subtitle'] )
			{
				echo wpautop( $instance['subtitle'] );
			}
			?>
			<div class="row section__content">
				<?php // Posts with big thumbs ?>
				<?php for ( $i = 1; $i < 3; $i ++ ) : ?>
					<?php if ( $query->have_posts() ) : ?>
						<?php $query->the_post(); ?>
						<article class="column one-third">
							<div class="entry-media">
								<a href="<?php the_permalink(); ?>" class="news__thumb"><?php the_post_thumbnail( 'induspress-grid-thumbnail' ); ?></a>
							</div>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?><span class="news__arrow">&#10142;</span></a></h3>
							<?php the_content(); ?>
						</article>
					<?php endif; ?>
				<?php endfor; ?>

				<?php // Posts with small thumbs ?>
				<div class="column one-third">
					<?php for ( $i = 1; $i < 5; $i ++ ) : ?>
						<?php if ( $query->have_posts() ) : ?>
							<?php $query->the_post(); ?>
							<article class="news__item clearfix">
								<div class="entry-media left">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'induspress-grid-thumbnail' ); ?></a>
								</div>
								<div class="entry-text">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<?php induspress_small_excerpt(); ?>
								</div>
							</article>
						<?php endif; ?>
					<?php endfor; ?>
				</div>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<?php
	}
}
