<?php
/**
 * Products widget: Displays list of product on the homepage.
 * @package IndusPress
 */

/**
 * Products widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Products extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'                 => '',
			'subtitle'              => '',
			'product_1_title'       => '',
			'product_1_description' => '',
			'product_1_image'       => '',
			'product_1_link'        => '#',
			'product_2_title'       => '',
			'product_2_description' => '',
			'product_2_image'       => '',
			'product_2_link'        => '#',
			'product_3_title'       => '',
			'product_3_description' => '',
			'product_3_image'       => '',
			'product_3_link'        => '#',
			'product_4_title'       => '',
			'product_4_description' => '',
			'product_4_image'       => '',
			'product_4_link'        => '#',
		);
		parent::__construct( 'induspress_products', __( 'IndusPress: Products', 'induspress' ), array(
			'classname'   => 'products',
			'description' => __( 'Displays list of products on the homepage.', 'induspress' ),
		) );

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_style( 'induspress-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'induspress-widget-image', get_template_directory_uri() . '/js/widget-image.js', array( 'jquery', 'media-upload', 'media-views' ), '', true );
		wp_localize_script( 'induspress-widget-image', 'IndusPressWidgetImage', array(
			'title'  => __( 'Select an image', 'induspress' ),
			'button' => __( 'Insert into widget', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		if ( $instance['title'] )
		{
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		if ( $instance['subtitle'] )
		{
			echo wpautop( $instance['subtitle'] );
		}
		?>
		<div class="container clearfix">
			<div class="row section__content">
				<?php for ( $i = 1; $i <= 4; $i ++ ) : ?>
					<div class="one-fourth column">
						<div class="entry-media">
							<a href="<?php echo esc_url( $instance["product_{$i}_link"] ); ?>"><img src="<?php echo esc_url( $instance["product_{$i}_image"] ); ?>"></a>
						</div>
						<h3>
							<a href="<?php echo esc_url( $instance["product_{$i}_link"] ); ?>"><?php echo $instance["product_{$i}_title"]; ?></a>
						</h3>
						<?php echo wpautop( $instance["product_{$i}_description"] ); ?>
					</div>
				<?php endfor; ?>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );
		for ( $i = 1; $i <= 4; $i ++ )
		{
			$instance["product_{$i}_title"]       = wp_kses_post( $new_instance["product_{$i}_title"] );// HTML is allowed
			$instance["product_{$i}_description"] = wp_kses_post( $new_instance["product_{$i}_description"] ); // HTML is allowed
			$instance["product_{$i}_image"]       = $this->sanitize_image( $new_instance["product_{$i}_image"] );
			$instance["product_{$i}_link"]        = wp_kses_post( $new_instance["product_{$i}_link"] );
		}
		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<?php
		for ( $i = 1; $i <= 4; $i ++ )
		{
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "product_{$i}_title" ) ); ?>"><?php printf( esc_html__( 'Product %d title', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "product_{$i}_title" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "product_{$i}_title" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["product_{$i}_title"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "product_{$i}_link" ) ); ?>"><?php printf( esc_html__( 'Product %d link', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "product_{$i}_link" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "product_{$i}_link" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["product_{$i}_link"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "product_{$i}_description" ) ); ?>"><?php printf( esc_html__( 'Product %d description', 'induspress' ), $i ); ?></label>
				<textarea id="<?php echo esc_attr( $this->get_field_id( "product_{$i}_description" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "product_{$i}_description" ) ); ?>" class="widefat"><?php echo esc_textarea( $instance["product_{$i}_description"] ); ?></textarea>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php printf( esc_html__( 'Product %d image', 'induspress' ), $i ); ?></label>
				<span class="induspress-widget-image">
					<input id="<?php echo esc_attr( $this->get_field_id( "product_{$i}_image" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "product_{$i}_image" ) ); ?>" type="text" class="induspress-widget-image__input" value="<?php echo esc_attr( $instance["product_{$i}_image"] ); ?>">
					<button class="button induspress-widget-image__select"><?php esc_html_e( 'Select' ); ?></button>
					<img src="<?php echo esc_url( $instance["product_{$i}_image"] ); ?>" class="induspress-widget-image__image<?php echo $instance["product_{$i}_image"] ? '' : ' hidden'; ?>">
				</span>
			</p>
			<?php
		}
	}

	/**
	 * Sanitize image URL before saving into the database.
	 * @param string $url Image URL
	 * @return string Sanitized URL
	 */
	public function sanitize_image( $url )
	{
		// Check valid URL
		$url = esc_url_raw( $url );

		// Check image extension
		$file = wp_check_filetype( $url, array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
			'bmp'          => 'image/bmp',
			'tif|tiff'     => 'image/tiff',
			'ico'          => 'image/x-icon',
		) );
		return $file['ext'] ? $url : '';
	}
}
