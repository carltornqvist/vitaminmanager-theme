<?php
/**
 * Dribbble widget: displays Dribbble portfolio.
 * @package IndusPress
 */

include_once ABSPATH . WPINC . '/feed.php';

/**
 * Dribbble widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Dribbble extends WP_Widget
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		$widget_ops = array(
			'classname'   => 'induspress_dribbble_widget',
			'description' => __( 'Displays your latest Dribbble photos.', 'induspress' )
		);
		parent::__construct( 'induspress_dribbble_widget', __( 'IndusPress: Widget Dribbble', 'induspress' ), $widget_ops );
	}

	/**
	 * Display widget
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		extract( $args );

		$title    = apply_filters( 'title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$username = $instance['username'];
		$number   = $instance['number'];

		echo $args['before_widget'];

		if ( ! empty( $title ) )
		{
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$rss = fetch_feed( "http://dribbble.com/$username/shots.rss" );
		add_filter( 'wp_feed_cache_transient_lifetime', create_function( '$a', 'return 1800;' ) );
		if ( ! is_wp_error( $rss ) )
		{
			$items = $rss->get_items( 0, $rss->get_item_quantity( $number ) );
		}

		if ( ! empty( $items ) ): ?>

			<div class="dribbble-container">

				<?php foreach ( $items as $item ): ?>
					<?php
					$title       = $item->get_title();
					$link        = $item->get_permalink();
					$description = $item->get_description();

					preg_match( "/src=\"(http.*(jpg|jpeg|gif|png))/", $description, $image_url );
					$image = $image_url[1];
					?>

					<a href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( $title ); ?>" class="dribbble-shot"><img src="<?php echo esc_url( $image ); ?>" alt="<?php echo esc_attr( $title ); ?>"></a>

				<?php endforeach; ?>
			</div>

		<?php endif;

		echo $args['after_widget'];
	}

	/**
	 * Update the widget parameters
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance             = $old_instance;
		$instance['title']    = sanitize_text_field( $new_instance['title'] );
		$instance['username'] = sanitize_text_field( $new_instance['username'] );
		$instance['number']   = absint( $new_instance['number'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin
	 * @param array $instance Widget instance parameter
	 * @return void
	 */
	public function form( $instance )
	{
		// Get the options into variables, escaping html characters on the way
		$title    = isset( $instance['title'] ) ? $instance['title'] : '';
		$username = isset( $instance['username'] ) ? $instance['username'] : '';
		$number   = isset( $instance['number'] ) ? absint( $instance['number'] ) : 4;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Dribbble username', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $username ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of images to display', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" class="widefat" value="<?php echo absint( $number ); ?>">
		</p>
		<?php
	}
}
