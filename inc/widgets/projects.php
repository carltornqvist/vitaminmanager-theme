<?php
/**
 * Projects widget: Displays list of projects on the homepage.
 * @package IndusPress
 */

/**
 * Projects widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Projects extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'                 => '',
			'subtitle'              => '',
			'project_1_title'       => '',
			'project_1_description' => '',
			'project_1_image'       => '',
			'project_1_link'        => '#',
			'project_1_date'        => '',
			'project_1_address'     => '',
			'project_1_price'       => '',
			'project_2_title'       => '',
			'project_2_description' => '',
			'project_2_image'       => '',
			'project_2_link'        => '#',
			'project_2_date'        => '',
			'project_2_address'     => '',
			'project_2_price'       => '',
			'project_3_title'       => '',
			'project_3_description' => '',
			'project_3_image'       => '',
			'project_3_link'        => '#',
			'project_3_date'        => '',
			'project_3_address'     => '',
			'project_3_price'       => '',
			'project_4_title'       => '',
			'project_4_description' => '',
			'project_4_image'       => '',
			'project_4_link'        => '#',
			'project_4_date'        => '',
			'project_4_address'     => '',
			'project_4_price'       => '',
		);
		parent::__construct( 'induspress_projects', __( 'IndusPress: Projects', 'induspress' ), array(
			'classname'   => 'section--dark projects',
			'description' => __( 'Displays list of projects on the homepage.', 'induspress' ),
		) );

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_style( 'induspress-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'induspress-widget-image', get_template_directory_uri() . '/js/widget-image.js', array( 'jquery', 'media-upload', 'media-views' ), '', true );
		wp_localize_script( 'induspress-widget-image', 'IndusPressWidgetImage', array(
			'title'  => __( 'Select an image', 'induspress' ),
			'button' => __( 'Insert into widget', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		if ( $instance['title'] )
		{
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		if ( $instance['subtitle'] )
		{
			echo wpautop( $instance['subtitle'] );
		}
		?>
		<div class="container clearfix">
			<div class="section__content thumb-text grid collapse">
				<?php for ( $i = 1; $i <= 4; $i ++ ) : ?>
					<article class="one-half column">
						<div class="entry-media">
							<a href="<?php echo esc_url( $instance["project_{$i}_link"] ); ?>"><img src="<?php echo esc_url( $instance["project_{$i}_image"] ); ?>"></a>
						</div>

						<div class="entry-text">
							<header class="entry-header">
								<div class="entry-meta">
									<time class="entry-date published" datetime="<?php echo $instance["project_{$i}_date"]; ?>"><?php echo $instance["project_{$i}_date"]; ?></time>
								</div>
								<h3 class="entry-title">
									<a href="<?php echo esc_url( $instance["project_{$i}_link"] ); ?>"><?php echo $instance["project_{$i}_title"]; ?></a>
								</h3>
							</header>

							<div class="entry-content clearfix">
								<?php echo wpautop( $instance["project_{$i}_description"] ); ?>
								<?php if ( $instance["project_{$i}_address"] ) : ?>
									<p class="projects__location">
										<i class="icon icon-location"></i><?php echo $instance["project_{$i}_address"]; ?>
									</p>
								<?php endif; ?>
								<?php if ( $instance["project_{$i}_price"] ) : ?>
									<p class="projects__price">
										<i class="icon icon-shopping-cart"></i><?php echo $instance["project_{$i}_price"]; ?>
									</p>
								<?php endif; ?>
							</div>
						</div>
					</article>
				<?php endfor; ?>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );
		for ( $i = 1; $i <= 4; $i ++ )
		{
			$instance["project_{$i}_title"]       = wp_kses_post( $new_instance["project_{$i}_title"] );// HTML is allowed
			$instance["project_{$i}_description"] = wp_kses_post( $new_instance["project_{$i}_description"] ); // HTML is allowed
			$instance["project_{$i}_image"]       = $this->sanitize_image( $new_instance["project_{$i}_image"] );
			$instance["project_{$i}_link"]        = esc_url_raw( $new_instance["project_{$i}_link"] );
			$instance["project_{$i}_date"]        = sanitize_text_field( $new_instance["project_{$i}_date"] );
			$instance["project_{$i}_address"]     = sanitize_text_field( $new_instance["project_{$i}_address"] );// HTML is allowed
			$instance["project_{$i}_price"]       = sanitize_text_field( $new_instance["project_{$i}_price"] );// HTML is allowed
		}
		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<?php
		for ( $i = 1; $i <= 4; $i ++ )
		{
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_title" ) ); ?>"><?php printf( esc_html__( 'Project %d title', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_title" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_title" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["project_{$i}_title"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_link" ) ); ?>"><?php printf( esc_html__( 'Project %d link', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_link" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_link" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["project_{$i}_link"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_description" ) ); ?>"><?php printf( esc_html__( 'Project %d description', 'induspress' ), $i ); ?></label>
				<textarea id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_description" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_description" ) ); ?>" class="widefat"><?php echo esc_textarea( $instance["project_{$i}_description"] ); ?></textarea>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php printf( esc_html__( 'Project %d image', 'induspress' ), $i ); ?></label>
				<span class="induspress-widget-image">
					<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_image" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_image" ) ); ?>" type="text" class="induspress-widget-image__input" value="<?php echo esc_attr( $instance["project_{$i}_image"] ); ?>">
					<button class="button induspress-widget-image__select"><?php esc_html_e( 'Select' ); ?></button>
					<img src="<?php echo esc_url( $instance["project_{$i}_image"] ); ?>" class="induspress-widget-image__image<?php echo $instance["project_{$i}_image"] ? '' : ' hidden'; ?>">
				</span>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_date" ) ); ?>"><?php printf( esc_html__( 'Project %d date', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_date" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_date" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["project_{$i}_date"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_address" ) ); ?>"><?php printf( esc_html__( 'Project %d location', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_address" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_address" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["project_{$i}_address"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "project_{$i}_price" ) ); ?>"><?php printf( esc_html__( 'Project %d price', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "project_{$i}_price" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "project_{$i}_price" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["project_{$i}_price"] ); ?>">
			</p>
			<?php
		}
	}

	/**
	 * Sanitize image URL before saving into the database.
	 * @param string $url Image URL
	 * @return string Sanitized URL
	 */
	public function sanitize_image( $url )
	{
		// Check valid URL
		$url = esc_url_raw( $url );

		// Check image extension
		$file = wp_check_filetype( $url, array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
			'bmp'          => 'image/bmp',
			'tif|tiff'     => 'image/tiff',
			'ico'          => 'image/x-icon',
		) );
		return $file['ext'] ? $url : '';
	}
}
