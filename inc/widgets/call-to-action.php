<?php
/**
 * Call to action widget: Displays call to action on the homepage.
 * @package IndusPress
 */

/**
 * Call to action widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Call_To_Action extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'text'        => '',
			'button_text' => '',
			'button_link' => '#',
		);
		parent::__construct( 'induspress_call_to_action', __( 'IndusPress: Call to action', 'induspress' ), array(
			'classname'   => 'section--dark call-to-action',
			'description' => __( 'Displays call to action on the homepage.', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		?>
		<div class="container clearfix">
			<?php if ( $instance['text'] ) : ?>
				<h3><?php echo $instance['text']; ?></h3>
			<?php endif; ?>
			<?php if ( $instance['button_text'] && $instance['button_link'] ) : ?>
				<a href="<?php echo esc_url( $instance['button_link'] ); ?>" class="button-minimal right"><?php echo esc_html( $instance['button_text'] ); ?></a>
			<?php endif; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance                = $old_instance;
		$instance['text']        = wp_kses_post( $new_instance['text'] ); // HTML is allowed
		$instance['button_text'] = sanitize_text_field( $new_instance['button_text'] );
		$instance['button_link'] = esc_url_raw( $new_instance['button_link'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php esc_html_e( 'Text', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['text'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'button_link' ) ); ?>"><?php esc_html_e( 'Button link', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'button_link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_link' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['button_link'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>"><?php esc_html_e( 'Button text', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'button_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button_text' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['button_text'] ); ?>">
		</p>
		<?php
	}
}
