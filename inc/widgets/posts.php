<?php
/**
 * Posts widget: Displays list of post with categories or tag.
 * @package IndusPress
 */

/**
 * Posts widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Posts extends WP_Widget
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		$widget_ops = array(
			'classname'   => 'induspress_posts_widget',
			'description' => __( 'Display latest posts from a category or a tag.', 'induspress' )
		);
		parent::__construct( 'induspress_posts', __( 'IndusPress: Posts', 'induspress' ), $widget_ops );
	}

	/**
	 * Display widget
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts', 'induspress' ) : $instance['title'], $instance, $this->id_base );

		$number = ! empty( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
		{
			$number = 5;
		}

		$show_thumbnail = isset( $instance['show_thumbnail'] ) ? $instance['show_thumbnail'] : true;
		$show_date      = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$arg = array(
			'posts_per_page' => $number,
			'post_status'    => 'publish',
		);

		if ( ! empty( $instance['category_tag'] ) )
		{
			$arg['cat'] = absint( $instance['category_tag'] );
		}

		$query = new WP_Query( $arg );
		if ( ! $query->have_posts() )
		{
			return;
		}

		echo $args['before_widget'];
		if ( $title )
		{
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>
		<ul>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<li class="<?php echo $show_thumbnail ? 'has-thumbnail' : ''; ?>">
					<?php if ( $show_thumbnail ) : ?>
						<div class="entry-media left">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="<?php the_ID(); ?>">
								<?php
								if ( has_post_thumbnail() )
								{
									the_post_thumbnail( 'thumbnail' );
								}
								else
								{
									echo '<img width="64" height="50" class="attachment-thumbnail wp-post-image" src="' . get_template_directory_uri() . '/img/no-sidebar.png">';
								}
								?>
							</a>
						</div>
					<?php endif; ?>
					<div class="entry-text">
						<h4 class="entry-title">
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h4>
						<?php if ( $show_date ) : ?>
							<div class="entry-meta">
								<?php echo induspress_entry_meta_element( 'published_date' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php
		echo $args['after_widget'];
		wp_reset_postdata();
	}

	/**
	 * Update widget parameters
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance                   = $old_instance;
		$instance['title']          = strip_tags( $new_instance['title'] );
		$instance['number']         = absint( $new_instance['number'] );
		$instance['category_tag']   = absint( $new_instance['category_tag'] );
		$instance['show_thumbnail'] = isset( $new_instance['show_thumbnail'] ) ? (bool) $new_instance['show_thumbnail'] : false;
		$instance['show_date']      = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;

		return $instance;
	}

	/**
	 * Display widget form in the admin
	 * @param array $instance Widget instance parameter
	 * @return void
	 */
	public function form( $instance )
	{
		$title          = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number         = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$category_tag   = isset( $instance['category_tag'] ) ? absint( $instance['category_tag'] ) : '';
		$show_thumbnail = isset( $instance['show_thumbnail'] ) ? (bool) $instance['show_thumbnail'] : false;
		$show_date      = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" class="widefat" value="<?php echo absint( $number ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'category_tag' ) ); ?>"><?php esc_html_e( 'A list post of categories or tag.:', 'induspress' ); ?></label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'category_tag' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'category_tag' ) ); ?>">
				<?php self::get_category_tag( $category_tag ); ?>
			</select>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_thumbnail ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_thumbnail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_thumbnail' ) ); ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_thumbnail' ) ); ?>"><?php esc_html_e( 'Display post thumbnail?', 'induspress' ); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>">
			<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display post date?', 'induspress' ); ?></label>
		</p>
		<?php
	}

	/**
	 * Get list category and tag
	 * @param int $selected Selected category or tag
	 */
	public static function get_category_tag( $selected )
	{
		$option     = '<option value="">' . __( 'Select', 'induspress' ) . '</option>';
		$categories = get_categories();
		$tags       = get_tags();

		if ( ! empty( $categories ) )
		{
			$option .= '<optgroup label="' . __( 'Categories', 'induspress' ) . '">';
			foreach ( $categories as $category )
			{
				$option .= sprintf(
					'<option value="%s" %s>%s</option>',
					$category->term_id,
					selected( $selected, $category->term_id, false ),
					$category->name
				);
			}
			$option .= '</optgroup>';
		}

		if ( ! empty( $tags ) )
		{
			$option .= '<optgroup label="' . __( 'Tags', 'induspress' ) . '">';
			foreach ( $tags as $tag )
			{
				$option .= sprintf(
					'<option value="%s" %s>%s</option>',
					$tag->term_id,
					selected( $selected, $tag->term_id, false ),
					$tag->name
				);
			}
			$option .= '</optgroup>';
		}

		echo $option;
	}
}
