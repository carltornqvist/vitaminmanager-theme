<?php
/**
 * Services widget: Displays list of services on the homepage.
 * @package IndusPress
 */

/**
 * Services widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Services extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'                 => '',
			'subtitle'              => '',
			'image'                 => '',
			'service_1_title'       => '',
			'service_1_description' => '',
			'service_1_image'       => '',
			'service_2_title'       => '',
			'service_2_description' => '',
			'service_2_image'       => '',
			'service_3_title'       => '',
			'service_3_description' => '',
			'service_3_image'       => '',
			'service_4_title'       => '',
			'service_4_description' => '',
			'service_4_image'       => '',
			'service_5_title'       => '',
			'service_5_description' => '',
			'service_5_image'       => '',
			'service_6_title'       => '',
			'service_6_description' => '',
			'service_6_image'       => '',
		);
		parent::__construct( 'induspress_services', __( 'IndusPress: Services', 'induspress' ), array(
			'classname'   => 'services',
			'description' => __( 'Displays list of services on the homepage.', 'induspress' ),
		) );

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_style( 'induspress-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'induspress-widget-image', get_template_directory_uri() . '/js/widget-image.js', array( 'jquery', 'media-upload', 'media-views' ), '', true );
		wp_localize_script( 'induspress-widget-image', 'IndusPressWidgetImage', array(
			'title'  => __( 'Select an image', 'induspress' ),
			'button' => __( 'Insert into widget', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		if ( $instance['title'] )
		{
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		if ( $instance['subtitle'] )
		{
			echo wpautop( $instance['subtitle'] );
		}
		?>
		<ul class="services__items section__content" style="background: url(<?php echo esc_url( $instance['image'] ); ?>);">
			<?php
			for ( $i = 1; $i <= 6; $i ++ ) :
				$class = "services__item services__item--$i";
				if ( $i > 3 )
				{
					$class .= ' services__item--right';
				}
				?>
				<li class="<?php echo esc_attr( $class ); ?>">
					<div class="services__item__content clearfix">
						<img class="services__item__image" src="<?php echo esc_url( $instance["service_{$i}_image"] ); ?>">
						<div class="services__item__description">
							<h3><?php echo $instance["service_{$i}_title"]; ?></h3>
							<?php echo wpautop( $instance["service_{$i}_description"] ); ?>
						</div>
					</div>
				</li>
			<?php endfor; ?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );
		$instance['image']    = $this->sanitize_image( $new_instance['image'] );
		for ( $i = 1; $i <= 6; $i ++ )
		{
			$instance["service_{$i}_title"]       = wp_kses_post( $new_instance["service_{$i}_title"] );
			$instance["service_{$i}_description"] = wp_kses_post( $new_instance["service_{$i}_description"] );
			$instance["service_{$i}_image"]       = $this->sanitize_image( $new_instance["service_{$i}_image"] );
		}
		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_html_e( 'Center image', 'induspress' ); ?></label>
			<span class="induspress-widget-image">
				<input id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="text" class="induspress-widget-image__input" value="<?php echo esc_attr( $instance['image'] ); ?>">
				<button class="button induspress-widget-image__select"><?php esc_html_e( 'Select' ); ?></button>
				<img src="<?php echo esc_url( $instance['image'] ); ?>" class="induspress-widget-image__image<?php echo $instance['image'] ? '' : ' hidden'; ?>">
			</span>
		</p>
		<?php
		for ( $i = 1; $i <= 6; $i ++ )
		{
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "service_{$i}_title" ) ); ?>"><?php printf( esc_html__( 'Service %d title', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "service_{$i}_title" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "service_{$i}_title" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["service_{$i}_title"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "service_{$i}_description" ) ); ?>"><?php printf( esc_html__( 'Service %d description', 'induspress' ), $i ); ?></label>
				<textarea id="<?php echo esc_attr( $this->get_field_id( "service_{$i}_description" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "service_{$i}_description" ) ); ?>" class="widefat"><?php echo esc_textarea( $instance["service_{$i}_description"] ); ?></textarea>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php printf( esc_html__( 'Service %d image', 'induspress' ), $i ); ?></label>
				<span class="induspress-widget-image">
					<input id="<?php echo esc_attr( $this->get_field_id( "service_{$i}_image" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "service_{$i}_image" ) ); ?>" type="text" class="induspress-widget-image__input" value="<?php echo esc_attr( $instance["service_{$i}_image"] ); ?>">
					<button class="button induspress-widget-image__select"><?php esc_html_e( 'Select' ); ?></button>
					<img src="<?php echo esc_url( $instance["service_{$i}_image"] ); ?>" class="induspress-widget-image__image<?php echo $instance["service_{$i}_image"] ? '' : ' hidden'; ?>">
				</span>
			</p>
			<?php
		}
	}

	/**
	 * Sanitize image URL before saving into the database.
	 * @param string $url Image URL
	 * @return string Sanitized URL
	 */
	public function sanitize_image( $url )
	{
		// Check valid URL
		$url = esc_url_raw( $url );

		// Check image extension
		$file = wp_check_filetype( $url, array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
			'png'          => 'image/png',
			'bmp'          => 'image/bmp',
			'tif|tiff'     => 'image/tiff',
			'ico'          => 'image/x-icon',
		) );
		return $file['ext'] ? $url : '';
	}
}
