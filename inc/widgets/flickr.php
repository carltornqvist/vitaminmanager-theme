<?php
/**
 * Flickr widget: displays Flickr gallery.
 * @package IndusPress
 */

/**
 * Flickr widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Flickr extends WP_Widget
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		$widget_ops = array(
			'classname'   => 'induspress_flickr_widget',
			'description' => __( 'Displays your latest Flickr photos.', 'induspress' )
		);
		parent::__construct( 'induspress_flickr_widget', __( 'IndusPress: Widget Flickr', 'induspress' ), $widget_ops );
	}

	/**
	 * Display widget
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$title  = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$id     = $instance['id'];
		$number = $instance['number'];

		echo $args['before_widget'];

		if ( ! empty( $title ) )
		{
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>

		<script src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo absint( $number ); ?>&amp;display=latest&amp;size=m&amp;layout=x&amp;source=user&amp;user=<?php echo esc_attr( $id ); ?>"></script>

		<?php echo $args['after_widget'];
	}

	/**
	 * Update the widget parameters
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['id']     = sanitize_text_field( $new_instance['id'] );
		$instance['number'] = absint( $new_instance['number'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin
	 * @param array $instance Widget instance parameter
	 * @return void
	 */
	public function form( $instance )
	{
		// Get the options into variables, escaping html characters on the way
		$title  = isset( $instance['title'] ) ? $instance['title'] : '';
		$id     = isset( $instance['id'] ) ? sanitize_text_field( $instance['id'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 9;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>"><?php _e( 'Flickr ID (use <a target="_blank" href="http://www.idgettr.com">idGettr</a>)', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'id' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'id' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $id ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of images to display', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" class="widefat" value="<?php echo absint( $number ); ?>">
		</p>
		<?php
	}
}
