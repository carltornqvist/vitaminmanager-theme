<?php
/**
 * clients widget: Displays list of clients on the homepage.
 * @package IndusPress
 */

/**
 * clients widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Clients extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'    => '',
			'subtitle' => '',
			'clients'  => array(),
		);
		parent::__construct( 'induspress_clients', __( 'IndusPress: Clients', 'induspress' ), array(
			'classname'   => 'clients',
			'description' => __( 'Displays list of clients on the homepage.', 'induspress' ),
		) );

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_style( 'induspress-widget-image', get_template_directory_uri() . '/css/widget-image.css' );

		wp_enqueue_media();
		wp_enqueue_script( 'induspress-widget-image', get_template_directory_uri() . '/js/widget-image.js', array( 'jquery', 'media-upload', 'media-views' ), '', true );
		wp_localize_script( 'induspress-widget-image', 'IndusPressWidgetImage', array(
			'title'  => __( 'Select an image', 'induspress' ),
			'button' => __( 'Insert into widget', 'induspress' ),
		) );

		wp_enqueue_script( 'induspress-widget-clients', get_template_directory_uri() . '/js/widget-clients.js', array( 'induspress-widget-image' ), '', true );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		if ( $instance['title'] )
		{
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		if ( $instance['subtitle'] )
		{
			echo wpautop( $instance['subtitle'] );
		}
		?>
		<div class="container clearfix">
			<div class="owl-carousel">
				<?php foreach ( $instance['clients'] as $client ) : ?>
					<div class="item">
						<a href="<?php echo esc_url( $client['link'] ); ?>">
							<img src="<?php echo esc_url( $client['image'] ); ?>" alt="<?php echo esc_attr( $client['title'] ); ?>">
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );
		$instance['clients']  = array_values( $new_instance['clients'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		if ( empty( $instance['clients'] ) )
		{
			$instance['clients'][] = array(
				'title' => '',
				'image' => '',
				'link'  => '',
			);
		}
		// Reset index
		$instance['clients'] = array_values( $instance['clients'] );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<?php
		foreach ( $instance['clients'] as $k => $client ) :
			$name = esc_attr( $this->get_field_name( 'clients' ) . '[' . $k . '][%s]' );
			?>
			<div class="induspress-client">
				<p>
					<label>
						<?php esc_html_e( 'Client title', 'induspress' ); ?>
						<a href="#" class="induspress-client-remove<?php echo $k ? '' : ' hidden'; ?>">
							<small><?php _e( 'Remove', 'induspress' ); ?></small>
						</a>
					</label>
					<input name="<?php printf( $name, 'title' ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $client['title'] ); ?>">
				</p>
				<p>
					<label><?php esc_html_e( 'Client link', 'induspress' ); ?></label>
					<input name="<?php printf( $name, 'link' ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $client['link'] ); ?>">
				</p>
				<p>
					<label><?php esc_html_e( 'Client image', 'induspress' ); ?></label>
					<span class="induspress-widget-image">
						<input name="<?php printf( $name, 'image' ); ?>" type="text" class="induspress-widget-image__input" value="<?php echo esc_attr( $client['image'] ); ?>">
						<button class="button induspress-widget-image__select"><?php esc_html_e( 'Select' ); ?></button>
						<img src="<?php echo esc_url( $client['image'] ); ?>" class="induspress-widget-image__image<?php echo $client['image'] ? '' : ' hidden'; ?>">
					</span>
				</p>
			</div>
		<?php endforeach; ?>
		<button class="button-primary induspress-clients-add"><?php _e( '+ Add client', 'induspress' ); ?></button>
		<?php
	}
}
