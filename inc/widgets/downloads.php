<?php
/**
 * downloads widget: Displays list of download.
 * @package IndusPress
 */

/**
 * downloads widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Downloads extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'    => '',
			'download'  => array(),
		);
		parent::__construct( 'induspress_downloads', __( 'IndusPress: Downloads', 'induspress' ), array(
			'classname'   => 'accordion-section',
			'description' => __( 'Displays list of download.', 'induspress' ),
		) );

		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_script( 'induspress-widget-clients', get_template_directory_uri() . '/js/widget-clients.js', array( 'induspress-widget-image' ), '', true );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		if ( $instance['title'] )
		{
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		?>
		<ul class="download_list">
			<?php foreach ( $instance['downloads'] as $download ) : ?>
				<li>
					<a class="download" href="<?php echo esc_url( $download['link'] ); ?>">
						<i class="icon icon-download icon_small"></i>
						<?php echo esc_attr( $download['title'] ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['downloads']  = array_values( $new_instance['downloads'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		if ( empty( $instance['downloads'] ) )
		{
			$instance['downloads'][] = array(
				'title' => '',
				'link'  => '',
			);
		}
		// Reset index
		$instance['downloads'] = array_values( $instance['downloads'] );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<?php
		foreach ( $instance['downloads'] as $k => $download ) :
			$name = esc_attr( $this->get_field_name( 'downloads' ) . '[' . $k . '][%s]' );
			?>
			<div class="induspress-client">
				<p>
					<label>
						<?php esc_html_e( 'Download title', 'induspress' ); ?>
						<a href="#" class="induspress-client-remove<?php echo $k ? '' : ' hidden'; ?>">
							<small><?php _e( 'Remove', 'induspress' ); ?></small>
						</a>
					</label>
					<input name="<?php printf( $name, 'title' ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $download['title'] ); ?>">
				</p>
				<p>
					<label><?php esc_html_e( 'Download link', 'induspress' ); ?></label>
					<input name="<?php printf( $name, 'link' ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $download['link'] ); ?>">
				</p>
			</div>
		<?php endforeach; ?>
		<button class="button-primary induspress-clients-add"><?php _e( '+ Add download', 'induspress' ); ?></button>
	<?php
	}
}
