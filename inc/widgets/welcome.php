<?php
/**
 * Welcome widget: Displays welcome text on the homepage.
 * @package IndusPress
 */

/**
 * Welcome widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Welcome extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'title'               => '',
			'subtitle'            => '',
			'main_title'          => '',
			'main_description'    => '',
			'block_1_title'       => '',
			'block_1_description' => '',
			'block_2_title'       => '',
			'block_2_description' => '',
			'block_3_title'       => '',
			'block_3_description' => '',
			'block_4_title'       => '',
			'block_4_description' => '',
		);
		parent::__construct( 'induspress_welcome', __( 'IndusPress: Welcome', 'induspress' ), array(
			'classname'   => 'section--dark welcome',
			'description' => __( 'Displays welcome text on the homepage.', 'induspress' ),
		) );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		?>
		<div class="container">
			<?php
			if ( $instance['title'] )
			{
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
			}
			if ( $instance['subtitle'] )
			{
				echo wpautop( $instance['subtitle'] );
			}
			?>
			<div class="grid section__content">
				<div class="one-third column">
					<h3><?php echo $instance['main_title']; ?></h3>
					<?php echo wpautop( $instance['main_description'] ); ?>
				</div>
				<div class="two-third column">
					<?php for ( $i = 1; $i <= 4; $i ++ ) : ?>
						<div class="one-half column">
							<h3><?php echo $instance["block_{$i}_title"]; ?></h3>
							<?php echo wpautop( $instance["block_{$i}_description"] ); ?>
						</div>
					<?php endfor; ?>
				</div>
			</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance                     = $old_instance;

		// HTML is allowed
		$instance['title']            = wp_kses_post( $new_instance['title'] );
		$instance['subtitle']         = wp_kses_post( $new_instance['subtitle'] );
		$instance['main_title']       = wp_kses_post( $new_instance['main_title'] );
		$instance['main_description'] = wp_kses_post( $new_instance['main_description'] );
		for ( $i = 1; $i <= 4; $i ++ )
		{
			$instance["block_{$i}_title"]       = wp_kses_post( $new_instance["block_{$i}_title"] );
			$instance["block_{$i}_description"] = wp_kses_post( $new_instance["block_{$i}_description"] );
		}
		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'main_title' ) ); ?>"><?php esc_html_e( 'Main block title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'main_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'main_title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['main_title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'main_description' ) ); ?>"><?php esc_html_e( 'Main block description', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'main_description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'main_description' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['main_description'] ); ?></textarea>
		</p>
		<?php
		for ( $i = 1; $i <= 4; $i ++ )
		{
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "block_{$i}_title" ) ); ?>"><?php printf( esc_html__( 'Block %d title', 'induspress' ), $i ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( "block_{$i}_title" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "block_{$i}_title" ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance["block_{$i}_title"] ); ?>">
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( "block_{$i}_description" ) ); ?>"><?php printf( esc_html__( 'Block %d description', 'induspress' ), $i ); ?></label>
				<textarea id="<?php echo esc_attr( $this->get_field_id( "block_{$i}_description" ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( "block_{$i}_description" ) ); ?>" class="widefat"><?php echo esc_textarea( $instance["block_{$i}_description"] ); ?></textarea>
			</p>
			<?php
		}
	}
}
