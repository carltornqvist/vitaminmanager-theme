<?php
/**
 * Contact info widget: Displays contact info in the header.
 * @package IndusPress
 */

/**
 * Contact info widget class.
 * @package IndusPress
 */
class IndusPress_Widget_Contact_Info extends WP_Widget
{
	/**
	 * Default widget parameters.
	 * @var array
	 */
	public $defaults;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		$this->defaults = array(
			'icon'     => '',
			'title'    => '',
			'subtitle' => '',
		);
		parent::__construct( 'induspress_contact-info', __( 'IndusPress: Contact info', 'induspress' ), array(
			'classname'   => 'contact-info',
			'description' => __( 'Displays contact info.', 'induspress' ),
		) );
		add_action( 'sidebar_admin_setup', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Enqueue script for image upload in widgets.
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_style( 'induspress-entypo', get_template_directory_uri() . '/css/entypo.css', '', '2.0' );
		wp_enqueue_style( 'induspress-widget-icon', get_template_directory_uri() . '/css/widget-icon.css' );
		wp_enqueue_script( 'induspress-widget-icon', get_template_directory_uri() . '/js/widget-icon.js', array( 'jquery' ), '', true );
	}

	/**
	 * Display widget.
	 * @param array $args     Sidebar arguments
	 * @param array $instance Widget instance parameters
	 */
	public function widget( $args, $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		echo $args['before_widget'];
		?>
		<i class="contact-info__icon icon <?php echo esc_attr( $instance['icon'] ); ?> icon-3x left"></i>
		<div class="contact-info__text left">
			<span class="contact-info__title"><?php echo $instance['title']; ?></span>
			<span class="contact-info__subtitle"><?php echo $instance['subtitle']; ?></span>
		</div>
		<?php
		echo $args['after_widget'];
	}

	/**
	 * Update widget parameters.
	 * @param array $new_instance
	 * @param array $old_instance
	 * @return array
	 */
	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		// HTML is allowed
		$instance['icon']     = sanitize_text_field( $new_instance['icon'] );
		$instance['title']    = wp_kses_post( $new_instance['title'] );
		$instance['subtitle'] = wp_kses_post( $new_instance['subtitle'] );

		return $instance;
	}

	/**
	 * Display widget form in the admin.
	 * @param array $instance Widget instance parameter
	 * @return string|void
	 */
	public function form( $instance )
	{
		$instance = wp_parse_args( $instance, $this->defaults );
		$icons    = self::get_icons();
		?>
		<p>
			<label><?php esc_html_e( 'Icon', 'induspress' ); ?></label>
			<span class="induspress-widget-icon">
				<?php
				foreach ( $icons as $icon )
				{
					printf(
						'<input class="induspress-widget-icon__input" id="%s" type="radio" name="%s" value="%s" %s>
						<label for="%s" class="induspress-widget-icon__label"><i class="icon %s"></i></label>',
						esc_attr( $this->get_field_id( 'icon' ) . "-$icon" ),
						esc_attr( $this->get_field_name( 'icon' ) ),
						$icon,
						checked( $icon, $instance['icon'], false ),
						esc_attr( $this->get_field_id( 'icon' ) . "-$icon" ),
						$icon
					);
				}
				?>
			</span>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'induspress' ); ?></label>
			<input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" class="widefat" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>"><?php esc_html_e( 'Subtitle', 'induspress' ); ?></label>
			<textarea id="<?php echo esc_attr( $this->get_field_id( 'subtitle' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'subtitle' ) ); ?>" class="widefat"><?php echo esc_textarea( $instance['subtitle'] ); ?></textarea>
		</p>
		<?php
	}

	/**
	 * Get list of icons.
	 * @return array Array of class names
	 */
	static public function get_icons()
	{
		return array(
			'icon-add-to-list',
			'icon-classic-computer',
			'icon-controller-fast-backward',
			'icon-creative-commons-attribution',
			'icon-creative-commons-noderivs',
			'icon-creative-commons-noncommercial-eu',
			'icon-creative-commons-noncommercial-us',
			'icon-creative-commons-public-domain',
			'icon-creative-commons-remix',
			'icon-creative-commons-share',
			'icon-creative-commons-sharealike',
			'icon-creative-commons',
			'icon-document-landscape',
			'icon-remove-user',
			'icon-warning',
			'icon-arrow-bold-down',
			'icon-arrow-bold-left',
			'icon-arrow-bold-right',
			'icon-arrow-bold-up',
			'icon-arrow-down',
			'icon-arrow-left',
			'icon-arrow-long-down',
			'icon-arrow-long-left',
			'icon-arrow-long-right',
			'icon-arrow-long-up',
			'icon-arrow-right',
			'icon-arrow-up',
			'icon-arrow-with-circle-down',
			'icon-arrow-with-circle-left',
			'icon-arrow-with-circle-right',
			'icon-arrow-with-circle-up',
			'icon-bookmark',
			'icon-bookmarks',
			'icon-chevron-down',
			'icon-chevron-left',
			'icon-chevron-right',
			'icon-chevron-small-down',
			'icon-chevron-small-left',
			'icon-chevron-small-right',
			'icon-chevron-small-up',
			'icon-chevron-thin-down',
			'icon-chevron-thin-left',
			'icon-chevron-thin-right',
			'icon-chevron-thin-up',
			'icon-chevron-up',
			'icon-chevron-with-circle-down',
			'icon-chevron-with-circle-left',
			'icon-chevron-with-circle-right',
			'icon-chevron-with-circle-up',
			'icon-cloud',
			'icon-controller-fast-forward',
			'icon-controller-jump-to-start',
			'icon-controller-next',
			'icon-controller-paus',
			'icon-controller-play',
			'icon-controller-record',
			'icon-controller-stop',
			'icon-controller-volume',
			'icon-dot-single',
			'icon-dots-three-horizontal',
			'icon-dots-three-vertical',
			'icon-dots-two-horizontal',
			'icon-dots-two-vertical',
			'icon-download',
			'icon-emoji-flirt',
			'icon-flow-branch',
			'icon-flow-cascade',
			'icon-flow-line',
			'icon-flow-parallel',
			'icon-flow-tree',
			'icon-install',
			'icon-layers',
			'icon-open-book',
			'icon-resize-100',
			'icon-resize-full-screen',
			'icon-save',
			'icon-select-arrows',
			'icon-sound-mute',
			'icon-sound',
			'icon-trash',
			'icon-triangle-down',
			'icon-triangle-left',
			'icon-triangle-right',
			'icon-triangle-up',
			'icon-uninstall',
			'icon-upload-to-cloud',
			'icon-upload',
			'icon-add-user',
			'icon-address',
			'icon-adjust',
			'icon-air',
			'icon-aircraft-landing',
			'icon-aircraft-take-off',
			'icon-aircraft',
			'icon-align-bottom',
			'icon-align-horizontal-middle',
			'icon-align-left',
			'icon-align-right',
			'icon-align-top',
			'icon-align-vertical-middle',
			'icon-archive',
			'icon-area-graph',
			'icon-attachment',
			'icon-awareness-ribbon',
			'icon-back-in-time',
			'icon-back',
			'icon-bar-graph',
			'icon-battery',
			'icon-beamed-note',
			'icon-bell',
			'icon-blackboard',
			'icon-block',
			'icon-book',
			'icon-bowl',
			'icon-box',
			'icon-briefcase',
			'icon-browser',
			'icon-brush',
			'icon-bucket',
			'icon-cake',
			'icon-calculator',
			'icon-calendar',
			'icon-camera',
			'icon-ccw',
			'icon-chat',
			'icon-check',
			'icon-circle-with-cross',
			'icon-circle-with-minus',
			'icon-circle-with-plus',
			'icon-circle',
			'icon-circular-graph',
			'icon-clapperboard',
			'icon-clipboard',
			'icon-clock',
			'icon-code',
			'icon-cog',
			'icon-colours',
			'icon-compass',
			'icon-copy',
			'icon-credit-card',
			'icon-credit',
			'icon-cross',
			'icon-cup',
			'icon-cw',
			'icon-cycle',
			'icon-database',
			'icon-dial-pad',
			'icon-direction',
			'icon-document',
			'icon-documents',
			'icon-drink',
			'icon-drive',
			'icon-drop',
			'icon-edit',
			'icon-email',
			'icon-emoji-happy',
			'icon-emoji-neutral',
			'icon-emoji-sad',
			'icon-erase',
			'icon-eraser',
			'icon-export',
			'icon-eye',
			'icon-feather',
			'icon-flag',
			'icon-flash',
			'icon-flashlight',
			'icon-flat-brush',
			'icon-folder-images',
			'icon-folder-music',
			'icon-folder-video',
			'icon-folder',
			'icon-forward',
			'icon-funnel',
			'icon-game-controller',
			'icon-gauge',
			'icon-globe',
			'icon-graduation-cap',
			'icon-grid',
			'icon-hair-cross',
			'icon-hand',
			'icon-heart-outlined',
			'icon-heart',
			'icon-help-with-circle',
			'icon-help',
			'icon-home',
			'icon-hour-glass',
			'icon-image-inverted',
			'icon-image',
			'icon-images',
			'icon-inbox',
			'icon-infinity',
			'icon-info-with-circle',
			'icon-info',
			'icon-key',
			'icon-keyboard',
			'icon-lab-flask',
			'icon-landline',
			'icon-language',
			'icon-laptop',
			'icon-leaf',
			'icon-level-down',
			'icon-level-up',
			'icon-lifebuoy',
			'icon-light-bulb',
			'icon-light-down',
			'icon-light-up',
			'icon-line-graph',
			'icon-link',
			'icon-list',
			'icon-location-pin',
			'icon-location',
			'icon-lock-open',
			'icon-lock',
			'icon-log-out',
			'icon-login',
			'icon-loop',
			'icon-magnet',
			'icon-magnifying-glass',
			'icon-mail',
			'icon-man',
			'icon-map',
			'icon-mask',
			'icon-medal',
			'icon-megaphone',
			'icon-menu',
			'icon-message',
			'icon-mic',
			'icon-minus',
			'icon-mobile',
			'icon-modern-mic',
			'icon-moon',
			'icon-mouse',
			'icon-music',
			'icon-network',
			'icon-new-message',
			'icon-new',
			'icon-news',
			'icon-note',
			'icon-notification',
			'icon-old-mobile',
			'icon-old-phone',
			'icon-palette',
			'icon-paper-plane',
			'icon-pencil',
			'icon-phone',
			'icon-pie-chart',
			'icon-pin',
			'icon-plus',
			'icon-popup',
			'icon-power-plug',
			'icon-price-ribbon',
			'icon-price-tag',
			'icon-print',
			'icon-progress-empty',
			'icon-progress-full',
			'icon-progress-one',
			'icon-progress-two',
			'icon-publish',
			'icon-quote',
			'icon-radio',
			'icon-reply-all',
			'icon-reply',
			'icon-retweet',
			'icon-rocket',
			'icon-round-brush',
			'icon-rss',
			'icon-ruler',
			'icon-scissors',
			'icon-share-alternitive',
			'icon-share',
			'icon-shareable',
			'icon-shield',
			'icon-shop',
			'icon-shopping-bag',
			'icon-shopping-basket',
			'icon-shopping-cart',
			'icon-shuffle',
			'icon-signal',
			'icon-sound-mix',
			'icon-sports-club',
			'icon-spreadsheet',
			'icon-squared-cross',
			'icon-squared-minus',
			'icon-squared-plus',
			'icon-star-outlined',
			'icon-star',
			'icon-stopwatch',
			'icon-suitcase',
			'icon-swap',
			'icon-sweden',
			'icon-switch',
			'icon-tablet',
			'icon-tag',
			'icon-text-document-inverted',
			'icon-text-document',
			'icon-text',
			'icon-thermometer',
			'icon-thumbs-down',
			'icon-thumbs-up',
			'icon-thunder-cloud',
			'icon-ticket',
			'icon-time-slot',
			'icon-tools',
			'icon-traffic-cone',
			'icon-tree',
			'icon-trophy',
			'icon-tv',
			'icon-typing',
			'icon-unread',
			'icon-untag',
			'icon-user',
			'icon-users',
			'icon-v-card',
			'icon-video',
			'icon-vinyl',
			'icon-voicemail',
			'icon-wallet',
			'icon-water',
			'icon-px-with-circle',
			'icon-px',
			'icon-basecamp',
			'icon-behance',
			'icon-creative-cloud',
			'icon-dropbox',
			'icon-evernote',
			'icon-flattr',
			'icon-foursquare',
			'icon-google-drive',
			'icon-google-hangouts',
			'icon-grooveshark',
			'icon-icloud',
			'icon-mixi',
			'icon-onedrive',
			'icon-paypal',
			'icon-picasa',
			'icon-qq',
			'icon-rdio-with-circle',
			'icon-renren',
			'icon-scribd',
			'icon-sina-weibo',
			'icon-skype-with-circle',
			'icon-skype',
			'icon-slideshare',
			'icon-smashing',
			'icon-soundcloud',
			'icon-spotify-with-circle',
			'icon-spotify',
			'icon-swarm',
			'icon-vine-with-circle',
			'icon-vine',
			'icon-vk-alternitive',
			'icon-vk-with-circle',
			'icon-vk',
			'icon-xing-with-circle',
			'icon-xing',
			'icon-yelp',
			'icon-dribbble-with-circle',
			'icon-dribbble',
			'icon-facebook-with-circle',
			'icon-facebook',
			'icon-flickr-with-circle',
			'icon-flickr',
			'icon-github-with-circle',
			'icon-github',
			'icon-google-with-circle',
			'icon-google',
			'icon-instagram-with-circle',
			'icon-instagram',
			'icon-lastfm-with-circle',
			'icon-lastfm',
			'icon-linkedin-with-circle',
			'icon-linkedin',
			'icon-pinterest-with-circle',
			'icon-pinterest',
			'icon-rdio',
			'icon-stumbleupon-with-circle',
			'icon-stumbleupon',
			'icon-tumblr-with-circle',
			'icon-tumblr',
			'icon-twitter-with-circle',
			'icon-twitter',
			'icon-vimeo-with-circle',
			'icon-vimeo',
			'icon-youtube-with-circle',
			'icon-youtube',
		);
	}
}
