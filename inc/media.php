<?php
/**
 * The main theme file which handles all media files in themes
 * @package IndusPress
 */

add_action( 'wp_enqueue_scripts', 'induspress_enqueue_scripts' );

/**
 * Enqueue scripts and styles
 */
function induspress_enqueue_scripts()
{
	wp_enqueue_style( 'induspress-fonts', induspress_google_fonts_url() );
	wp_enqueue_style( 'entypo', get_template_directory_uri() . '/css/entypo.css', '', '2.0' );

	/**
	 * Always load theme's style and allow child theme to add more style via style.css.
	 * @link http://justintadlock.com/archives/2014/11/03/loading-parent-styles-for-child-themes
	 */
	if ( is_child_theme() )
	{
		wp_enqueue_style( 'parent-style', trailingslashit( get_template_directory_uri() ) . 'style.css' );
	}
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	if ( is_front_page() )
	{
		wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.css' );
		wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array( 'jquery' ), '1.3.2', true );
	}
	wp_enqueue_script( 'baguetteBox', get_template_directory_uri() . '/js/baguetteBox.min.js', array( 'jquery' ), '1.4.2', true );
	wp_enqueue_script( 'induspress', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), wp_get_theme( 'induspress' )->version, true );

	// Comment threaded script
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
	{
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'init', 'induspress_add_editor_styles' );

/**
 * Add editor style
 */
function induspress_add_editor_styles()
{
	add_editor_style( array( 'css/editor-style.css', induspress_google_fonts_url(), get_template_directory_uri() . '/css/entypo.css' ) );
}

/**
 * Get Google Fonts URL
 * @return string
 */
function induspress_google_fonts_url()
{
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Roboto, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Roboto font: on or off', 'induspress' ) )
	{
		$fonts[] = 'Roboto:400,400italic,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Open Sans Condensed, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans Condensed font: on or off', 'induspress' ) )
	{
		$fonts[] = 'Open Sans Condensed:700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'induspress' );

	if ( 'cyrillic' == $subset )
	{
		$subsets .= ',cyrillic,cyrillic-ext';
	}
	elseif ( 'greek' == $subset )
	{
		$subsets .= ',greek,greek-ext';
	}
	elseif ( 'devanagari' == $subset )
	{
		$subsets .= ',devanagari';
	}
	elseif ( 'vietnamese' == $subset )
	{
		$subsets .= ',vietnamese';
	}

	if ( $fonts )
	{
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
