<?php
/**
 * Social buttons class.
 * @package IndusPress
 */

/**
 * Social buttons class
 * @package IndusPress
 */
class IndusPress_Social_Buttons
{
	/**
	 * Render social buttons
	 * @return void
	 */
	public function render()
	{
		echo '<span class="social-buttons">';
		$link = get_permalink();
		$text = get_the_title();
		$this->facebook( $link );
		$this->twitter( $link, $text );
		echo '</span>';
	}

	/**
	 * Generate HTML for a single Share Button
	 *
	 * @param string $link
	 *
	 * @return void
	 */
	public function facebook( $link )
	{
		printf(
			'<a class="facebook" target="_blank" title="%s" href="%s"><i class="icon icon-facebook"></i><span class="title">%s</span><span class="count"></span></a>',
			__( 'Dela på Facebook', 'induspress' ),
			htmlentities( add_query_arg( array(
				'u' => rawurlencode( $link ),
			), 'https://www.facebook.com/sharer/sharer.php' ) ),
			__( 'Dela på Facebook', 'induspress' )
		);
	}

	/**
	 * Generate HTML for a single Twitter Button
	 *
	 * @param string $link
	 * @param string $text
	 *
	 * @return void
	 */
	public function twitter( $link, $text )
	{
		printf(
			'<a class="twitter" target="_blank" title="%s" href="%s"><i class="icon icon-twitter"></i><span class="title">%s</span><span class="count"></span></a>',
			__( 'Share on Twitter', 'induspress' ),
			htmlentities( add_query_arg( array(
				'url'  => rawurlencode( $link ),
				'text' => rawurlencode( $text ),
			), 'https://twitter.com/intent/tweet' ) ),
			__( 'Share on Twitter', 'induspress' )
		);
	}

	/**
	 * Generate HTML for a single Google+ Button
	 *
	 * @param string $link
	 *
	 * @return void
	 */
	public function googleplus( $link )
	{
		printf(
			'<a class="googleplus" target="_blank" title="%s" href="%s"><i class="icon icon-google"></i><span class="title">%s</span><span class="count"></span></a>',
			__( 'Share on Google+', 'induspress' ),
			htmlentities( add_query_arg( array(
				'url' => rawurlencode( $link ),
			), 'https://plus.google.com/share' ) ),
			__( 'Share on Google+', 'induspress' )
		);
	}
}
