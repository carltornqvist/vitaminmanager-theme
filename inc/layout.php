<?php
/**
 * Add theme layout functionality.
 * @package IndusPress
 */

/**
 * Layout class.
 * @package IndusPress
 */
class IndusPress_Layout
{
	/**
	 * Add hooks
	 */
	public function __construct()
	{
		add_action( 'customize_register', array( $this, 'register' ) );
		add_filter( 'body_class', array( $this, 'layout_class' ) );
	}

	/**
	 * Register customizer settings
	 * @param WP_Customize_Manager $wp_customize WordPress customizer manager instance
	 */
	public function register( WP_Customize_Manager $wp_customize )
	{
		require_once get_template_directory() . '/inc/customizer/control-radio-image.php';
		require_once get_template_directory() . '/inc/customizer/sanitizer.php';

		$wp_customize->register_control_type( 'IndusPress_Customize_Control_Radio_Image' );
		$sanitizer = new IndusPress_Customize_Sanitizer;

		// Layout option
		$wp_customize->add_section( 'layout', array(
			'title' => __( 'Layout', 'induspress' ),
			'panel' => 'induspress',
		) );

		// Archive page layout style
		$wp_customize->add_setting( 'archive_layout_style', array(
			'default'           => induspress_default_setting( 'archive_layout_style' ),
			'sanitize_callback' => array( $sanitizer, 'select' ),
		) );
		$wp_customize->add_control( new IndusPress_Customize_Control_Radio_Image(
			$wp_customize,
			'archive_layout_style',
			array(
				'label'    => __( 'Archive page layout style', 'induspress' ),
				'section'  => 'layout',
				'type'     => 'radio-image',
				'choices'  => array(
					'list'       => array( 'url' => '%s/img/list.png', 'label' => __( 'List', 'induspress' ) ),
					'thumb-text' => array( 'url' => '%s/img/thumb-text.png', 'label' => __( 'Thumbnail Text', 'induspress' ) ),
				),
				'settings' => 'archive_layout_style',
			)
		) );

		// Archive page sidebar layout
		$wp_customize->add_setting( 'archive_sidebar_layout', array(
			'default'           => induspress_default_setting( 'archive_sidebar_layout' ),
			'sanitize_callback' => array( $sanitizer, 'select' ),
		) );
		$wp_customize->add_control( new IndusPress_Customize_Control_Radio_Image(
			$wp_customize,
			'archive_sidebar_layout',
			array(
				'label'    => __( 'Archive page sidebar layout', 'induspress' ),
				'section'  => 'layout',
				'type'     => 'radio-image',
				'choices'  => array(
					'no-sidebar' => array( 'url' => '%s/img/no-sidebar.png', 'label' => __( 'No Sidebar', 'induspress' ) ),
					'sidebar-left'       => array( 'url' => '%s/img/sidebar-left.png', 'label' => __( 'Sidebar Left', 'induspress' ) ),
					'sidebar-right'      => array( 'url' => '%s/img/sidebar-right.png', 'label' => __( 'Sidebar Right', 'induspress' ) ),
				),
				'settings' => 'archive_sidebar_layout',
			)
		) );

		// Page sidebar layout
		$wp_customize->add_setting( 'page_sidebar_layout', array(
			'default'           => induspress_default_setting( 'page_sidebar_layout' ),
			'sanitize_callback' => array( $sanitizer, 'select' ),
		) );
		$wp_customize->add_control( new IndusPress_Customize_Control_Radio_Image(
			$wp_customize,
			'page_sidebar_layout',
			array(
				'label'    => __( 'Page sidebar layout', 'induspress' ),
				'section'  => 'layout',
				'type'     => 'radio-image',
				'choices'  => array(
					'no-sidebar' => array( 'url' => '%s/img/no-sidebar.png', 'label' => __( 'No Sidebar', 'induspress' ) ),
					'sidebar-left'       => array( 'url' => '%s/img/sidebar-left.png', 'label' => __( 'Sidebar Left', 'induspress' ) ),
					'sidebar-right'      => array( 'url' => '%s/img/sidebar-right.png', 'label' => __( 'Sidebar Right', 'induspress' ) ),
				),
				'settings' => 'page_sidebar_layout',
			)
		) );

		// Single post sidebar layout
		$wp_customize->add_setting( 'single_sidebar_layout', array(
			'default'           => induspress_default_setting( 'single_sidebar_layout' ),
			'sanitize_callback' => array( $sanitizer, 'select' ),
		) );
		$wp_customize->add_control( new IndusPress_Customize_Control_Radio_Image(
			$wp_customize,
			'single_sidebar_layout',
			array(
				'label'    => __( 'Single post sidebar layout', 'induspress' ),
				'section'  => 'layout',
				'type'     => 'radio-image',
				'choices'  => array(
					'no-sidebar' => array( 'url' => '%s/img/no-sidebar.png', 'label' => __( 'No Sidebar', 'induspress' ) ),
					'sidebar-left'       => array( 'url' => '%s/img/sidebar-left.png', 'label' => __( 'Sidebar Left', 'induspress' ) ),
					'sidebar-right'      => array( 'url' => '%s/img/sidebar-right.png', 'label' => __( 'Sidebar Right', 'induspress' ) ),
				),
				'settings' => 'single_sidebar_layout',
			)
		) );

	}

	/**
	 * Get layout class for main content
	 *
	 * @param $class
	 * @return array
	 */
	public static function layout_class( $class )
	{
		if ( is_page() )
		{
			$class[] = induspress_setting( 'page_sidebar_layout' );
		}
		elseif ( is_single() )
		{
			$class[] = induspress_setting( 'single_sidebar_layout' );
		}
		elseif( is_home() || is_archive() || is_search() )
		{
			$class[] = induspress_setting( 'archive_sidebar_layout' );
			$class[]  = induspress_setting( 'archive_layout_style' );
		}

		return  $class;
	}
}
