<?php
/**
 * Add theme color scheme functionality.
 * @package IndusPress
 */

/**
 * Color scheme class.
 * @package IndusPress
 */
class IndusPress_Color_Scheme
{
	/**
	 * List of color options for color schemes.
	 * @var array
	 */
	public $options = array(
		'link_color',
		'button_background_color',
		'button_hover_background_color',
		'section_dark_background_color',
		'footer_background_color',
		'highlight_color',
		'section_title_background_color',
	);

	/**
	 * Detect if user select custom color scheme.
	 * @var bool
	 */
	public $is_custom = false;

	/**
	 * Add hooks
	 */
	public function __construct()
	{
		/**
		 * Add colors options to the Customizer.
		 * Priority 20 ensures the Theme Options panel is registered.
		 */
		add_action( 'customize_register', array( $this, 'customizer_register' ), 20 );
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'customize_js' ) );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'color_scheme_template' ) );
		add_action( 'customize_preview_init', array( $this, 'customize_preview_js' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'output_css' ) );
	}

	/**
	 * Register customizer settings
	 * @param WP_Customize_Manager $wp_customize WordPress customizer manager instance
	 */
	public function customizer_register( WP_Customize_Manager $wp_customize )
	{
		require_once get_template_directory() . '/inc/customizer/sanitizer.php';
		$sanitizer = new IndusPress_Customize_Sanitizer;

		$wp_customize->add_section( 'colors', array(
			'title' => __( 'Colors', 'induspress' ),
			'panel' => 'induspress',
		) );

		$color_schemes = $this->get_color_schemes();
		$choices       = array();
		foreach ( $color_schemes as $color_scheme => $value )
		{
			$choices[$color_scheme] = $value['label'];
		}
		$wp_customize->add_setting( 'color_scheme', array(
			'default'           => 'default',
			'sanitize_callback' => array( $sanitizer, 'select' ),
			'transport'         => 'postMessage',
		) );
		$wp_customize->add_control( 'color_scheme', array(
			'label'   => __( 'Color scheme', 'induspress' ),
			'section' => 'colors',
			'type'    => 'select',
			'choices' => $choices,
		) );

		$colors  = $this->get_color_scheme();
		$options = array(
			'link_color'                     => __( 'Link color', 'induspress' ),
			'button_background_color'        => __( 'Button background color', 'induspress' ),
			'button_hover_background_color'  => __( 'Button hover background color', 'induspress' ),
			'section_dark_background_color'  => __( 'Section dark background color', 'induspress' ),
			'footer_background_color'        => __( 'Footer background color', 'induspress' ),
			'highlight_color'                => __( 'Hightlight color', 'induspress' ),
			'section_title_background_color' => __( 'Section title background color', 'induspress' ),
		);
		$index   = 0;
		foreach ( $options as $key => $label )
		{
			$wp_customize->add_setting( $key, array(
				'default'           => $colors[$index],
				'sanitize_callback' => array( $sanitizer, 'hex_color' ),
				'transport'         => 'postMessage',
			) );
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $key, array(
				'label'   => $label,
				'section' => 'colors',
			) ) );
			$index ++;
		}
	}

	/**
	 * Enqueue Javascript for the Customizer control.
	 */
	public function customize_js()
	{
		wp_enqueue_script( 'induspress-color-scheme', get_template_directory_uri() . '/js/color-scheme.js', array( 'customize-controls', 'iris', 'underscore', 'wp-util' ), '', true );
		wp_localize_script( 'induspress-color-scheme', 'IndusPressColorScheme', $this->get_color_schemes() );
	}

	/**
	 * Outputs an Underscore template for generating CSS for the color scheme.
	 * The template generates the css dynamically for instant display in the Customizer preview.
	 */
	public function color_scheme_template()
	{
		$colors = array(
			'link_color'                     => '{{ data.link_color }}',
			'button_background_color'        => '{{ data.button_background_color }}',
			'button_hover_background_color'  => '{{ data.button_hover_background_color }}',
			'section_dark_background_color'  => '{{ data.section_dark_background_color }}',
			'footer_background_color'        => '{{ data.footer_background_color }}',
			'highlight_color'                => '{{ data.highlight_color }}',
			'section_title_background_color' => '{{ data.section_title_background_color }}',
		);
		?>
		<script type="text/html" id="tmpl-induspress-color-scheme">
			<?php echo $this->get_css( $colors ); ?>
		</script>
		<?php
	}

	/**
	 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
	 *
	 */
	public function customize_preview_js()
	{
		wp_enqueue_script( 'induspress-color-scheme-preview', get_template_directory_uri() . '/js/color-scheme-preview.js', array( 'customize-preview' ), '', true );
	}

	/**
	 * Output CSS for color schemes in the header.
	 */
	public function output_css()
	{
		$colors = $this->get_color_scheme();
		do_action( 'add_debug_info', $colors );
		if ( $this->is_custom )
		{
			$handle = is_child_theme() ? 'parent-style' : 'style';
			wp_add_inline_style( $handle, $this->get_css( $colors ) );
		}
	}

	/**
	 * Returns CSS for the color schemes.
	 * @param array $colors List of colors
	 * @return string Color scheme CSS.
	 */
	public function get_css( $colors )
	{
		$css = '
		/* Color Scheme */

		/* General */
		h1, h1 a,
		h2, h2 a,
		h3, h3 a,
		h4, h4 a,
		h5, h5 a,
		h6, a,
		a {
			color: %1$s;
		}
		blockquote {
			border-left-color: %2$s;
		}
		code, kbd, samp {
			color: %6$s;
		}
		button,
		input[type="submit"],
		input[type="reset"],
		input[type="button"],
		.button {
			background: %2$s;
		}
		button:hover,
		input[type="submit"]:hover,
		input[type="reset"]:hover,
		input[type="button"]:hover,
		.button:hover {
			background: %3$s;
		}
		.button-minimal:hover {
			color: %1$s;
		}

		/* Front-page */
		.section h2 span,
		.number {
			color: %6$s;
		}
		.section--dark,
		.services-1 {
			background-color: %4$s;
		}
		.welcome .button-minimal:hover {
			color: %1$s;
		}

		/* Menu */
		.main-menu > ul > li.menu-item-has-children > a:after,
		.main-menu > ul > li.page_item_has_children > a:after {
			border-top-color: %1$s;
		}
		.main-menu li li.menu-item-has-children > a:after,
		.main-menu li li.page_item_has_children > a:after {
			border-left-color: %1$s;
		}
		.main-menu li ul {
			border-top-color: %1$s;
		}

		/* Footer */
		.footer {
			background: %5$s;
		}

		/* Header */
		.brand,
		.contact-info__icon,
		.hero form {
			color: %1$s;
		}
		
		/* Header Title Section */
		
		.hero.entry-header {
			background: %7$s;	
		}

		/* Sidebar */
		.post-tags a:hover,
		.tagcloud a:hover {
			background: %2$s;
		}';
		return vsprintf( $css, $colors );
	}

	/**
	 * Get the current theme color scheme.
	 * @return array An associative array of either the current or default color scheme hex values.
	 */
	public function get_color_scheme()
	{
		// Get colors from predefined color schemes.
		$color_schemes = $this->get_color_schemes();
		$color_scheme  = get_theme_mod( 'color_scheme' );
		$color_scheme  = isset( $color_schemes[$color_scheme] ) ? $color_scheme : 'default';

		// Detect custom color scheme.
		if ( 'default' != $color_scheme )
		{
			$this->is_custom = true;
		}

		$colors = array_map( 'strtolower', $color_schemes[$color_scheme]['colors'] );

		// Get custom colors.
		foreach ( $this->options as $k => $option )
		{
			$color = get_theme_mod( $option );
			if ( $color && strtolower( $color ) != $colors[$k] )
			{
				$colors[$k] = $color;

				// Detect custom color scheme.
				$this->is_custom = true;
			}
		}
		return $colors;
	}

	/**
	 * Register color schemes for theme.
	 * The order of colors in a colors array:
	 * 1. Link Color.
	 * 2. Button Background Color.
	 * 3. Button Hover Background Color.
	 * 4. Section Dark Background Color.
	 * 5. Footer Background Color.
	 * 6. Highlight Color.
	 *
	 * @return array An associative array of color scheme options.
	 */
	public function get_color_schemes()
	{
		return array(
			'default' => array(
				'label'  => __( 'Default', 'induspress' ),
				'colors' => array(
					'#41535d',
					'#e67e22',
					'#c35b00',
					'#2c383f',
					'#222b30',
					'#e67e22',
					'#e67e22',
				),
			),
			'orange'    => array(
				'label'  => __( 'Orange', 'induspress' ),
				'colors' => array(
					'#dd8500',
					'#1d5d8e',
					'#00508e',
					'#aa6600',
					'#9d5f00',
					'#dd8500',
					'#dd8500',
				),
			),
			'green'  => array(
				'label'  => __( 'Green', 'induspress' ),
				'colors' => array(
					'#468966',
					'#b64926',
					'#8e2800',
					'#468966',
					'#2c5640',
					'#468966',
					'#468966',
				),
			),
			'pink'    => array(
				'label'  => __( 'Pink', 'induspress' ),
				'colors' => array(
					'#e53b51',
					'#e53b51',
					'#bc3042',
					'#ba3042',
					'#a52b3a',
					'#eeee22',
					'#eeee22',
				),
			),
			'purple'  => array(
				'label'  => __( 'Purple', 'induspress' ),
				'colors' => array(
					'#674970',
					'#27ae60',
					'#208f4f',
					'#674970',
					'#4b3551',
					'#27ae60',
					'#27ae60',
				),
			),
		);
	}
}
