<?php
/**
 * The template part for displaying sidebar.
 * @package IndusPress
 */
?>
<?php
if ( ! is_active_sidebar( 'primary' ) )
{
	return;
}

$class = IndusPress_Layout::layout_class( array() );

if ( in_array( 'no-sidebar', $class ) )
{
	return;
}
?>

<aside class="sidebar" role="complementary">
	<?php dynamic_sidebar( 'primary' ); ?>
</aside>
