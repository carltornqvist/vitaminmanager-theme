=== IndusPress ===
Contributors: Greta Themes
Requires at least: WordPress 4.1
Tested up to: WordPress 4.3.1
Version: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: light, white, cyan, two-columns, fluid-layout, responsive-layout, custom-header, custom-menu, featured-images, flexible-header, post-formats, sticky-post, threaded-comments, translation-ready, editor-style

== Description ==
IndusPress is a flexible and easy to use premium WordPress theme which can help you build on your own an incredible beautiful website with no time at all.This theme has thoughtful options for you to customize your website the easy way. It doesn’t confuse you with hundreds of options. Instead of that, all the options are carefully selected to optimize the user experience and make you less headache while still keep the maximum customizability.

Built with the latest technologies, IndusPress is compatible with all modern browsers, responsive in all screens and optimized for speed!

* Mobile-first, Responsive Layout
* Custom Header
* Social Links
* Post Formats

For more information about IndusPress please go to http://gretathemes.com/wordpress-themes/induspress/.

== Installation ==

Please go to http://gretathemes.com/docs/induspress-getting-started/

== Copyright ==

IndusPress WordPress Theme, Copyright 2015 Greta Themes

IndusPress is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Changelog ==

= 1.0.0 =
* Released: November 26, 2015

Initial release
