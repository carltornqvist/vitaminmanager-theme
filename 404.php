<?php
/**
 * The template part for displaying 404 page.
 * Content of this page (heading and search form) are moved to the header.
 * @package IndusPress
 */
?>

<?php get_header(); ?>
<?php get_footer(); ?>
