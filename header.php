<?php
/**
 * The template part for displaying header.
 * @package IndusPress
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<script type="text/javascript">
		(function () {
			if (document.addEventListener) {
				var d = new Date();
				var cb = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + "-" + d.getHours();
				var s = document.createElement("script");
				s.async = true;s.src = "//d191y0yd6d0jy4.cloudfront.net/sitegainer_5617289.js?cb="+cb;
				var s0 = document.getElementsByTagName("script")[0]; s0.parentNode.insertBefore(s, s0);
			}
		})();
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script type='text/javascript' src='https://widget.redeal.se/js/landing.js' async ></script>
<div class="wrapper">
	<?php get_template_part( 'template-parts/topbar' ); ?>
	<header class="header" role="banner">
	<?php get_template_part( 'template-parts/header', 'top' ); ?>
		<div class="container clearfix header-container">
			<?php
			$brand_text = '<span class="brand_text left">';
			$brand_text .= '<span class="brand_title">' . esc_html( get_bloginfo() ) . '</span>';
			$brand_text .= '<span class="brand_tagline">' . esc_html( get_bloginfo( 'description' ) ) . '</span>';
			$brand_text .= '</span>';
			$brand = $brand_text;
			if ( $logo = induspress_setting( 'logo' ) )
			{
				$brand = sprintf( '<img class="left" src="%s" alt="%s">', esc_url( $logo ), esc_attr( get_bloginfo() ) );
				if ( induspress_setting( 'display_site_title' ) )
				{
					$brand .= $brand_text;
				}
			}
			?>
			<a class="brand left logo-link" href="<?php echo esc_url( home_url() ); ?>"><?php echo $brand; ?></a>
			<button class="menu-toggle"><i class="icon icon-menu icon-3x"></i> <span><?php _e( 'Menu', 'induspress' ); ?></span></button>
			<?php if ( is_active_sidebar( 'header' ) ) : ?>
				<div class="header-widgets right">
					<?php dynamic_sidebar( 'header' ); ?>
				</div>
			<?php endif; ?>
		</div>
		<nav class="navbar container clearfix" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'induspress' ); ?>">
			<?php
			wp_nav_menu( array(
				'container_class' => 'main-menu',
				'menu_class'      => 'main-menu clearfix',
				'theme_location'  => 'primary',
				'items_wrap'      => '<ul>%3$s</ul>',
			) );
			?>
			<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'induspress' ); ?></a>
		</nav>
	</header>
	<?php get_template_part( 'template-parts/hero', is_front_page() ? 'front-page' : '' ); ?>
	<main class="main clearfix" role="main">
