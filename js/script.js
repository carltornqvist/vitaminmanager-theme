jQuery( function ( $ )
{

	/*
	Quick fix for jetpack form labels
	*/
	(function(){
		var jetPackLabel = $('.contact-form input[type="submit"]').val();
		if(jetPackLabel) $('.contact-form input[type="submit"]').val(jetPackLabel.replace('Submit', 'Skicka'));		
	})();

	var $window = $( window );

	$('li > a[href="https://vitaminmanager.com/shop/"]').click(function(ev) {
		ev.preventDefault();
	});
	/**
	 * Resize videos to fit the container
	 */
	function resizeVideo()
	{
		$( '.hentry iframe, .hentry object, .hentry video, .widget-content iframe, .widget-content object, .widget-content iframe' ).each( function ()
		{
			var $video = $( this ),
				$container = $video.parent(),
				containerWidth = $container.width(),
				$post = $video.closest( 'article' );

			if ( !$video.data( 'origwidth' ) )
			{
				$video.data( 'origwidth', $video.attr( 'width' ) );
				$video.data( 'origheight', $video.attr( 'height' ) );
			}
			var ratio = containerWidth / $video.data( 'origwidth' );
			$video.css( 'width', containerWidth + 'px' );

			// Only resize height for non-audio post format
			if ( !$post.hasClass( 'format-audio' ) )
			{
				$video.css( 'height', $video.data( 'origheight' ) * ratio + 'px' );
			}
		} );
	}

	/**
	 * Skip link to content
	 *
	 * @link : https://github.com/Automattic/_s/blob/master/js/skip-link-focus-fix.js
	 */
	function skipLinkFocusFix()
	{
		var is_webkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
			is_opera = navigator.userAgent.toLowerCase().indexOf( 'opera' ) > -1,
			is_ie = navigator.userAgent.toLowerCase().indexOf( 'msie' ) > -1;

		if ( ( is_webkit || is_opera || is_ie ) && document.getElementById && window.addEventListener )
		{
			window.addEventListener( 'hashchange', function ()
			{
				var id = location.hash.substring( 1 ),
					element;

				if ( !( /^[A-z0-9_-]+$/.test( id ) ) )
				{
					return;
				}

				element = document.getElementById( id );

				if ( element )
				{
					if ( !( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) )
					{
						element.tabIndex = -1;
					}

					element.focus();
				}
			}, false );
		}
	}

	/**
	 * Run slider for the front page.
	 */
	function slider()
	{
		$( '#hero-slider' ).owlCarousel( {
			autoPlay      : 5000,
			stopOnHover   : true,
			navigation    : true,
			pagination    : false,
			singleItem    : true,
			addClassActive: true,
			navigationText: ["<i class='icon icon-chevron-thin-left icon-4x'></i>", "<i class='icon icon-chevron-thin-right icon-4x'></i>"],
		} );
		$( '.clients .owl-carousel' ).owlCarousel( {
			autoPlay  : 3000,
			items     : 6,
			navigation: false,
			pagination: false
		} );
	}

	/**
	 * Show lightbox for gallery.
	 */
	function lightbox()
	{
		baguetteBox.run( '.gallery', {
			animation: 'fadeIn'
		} );
	}

	/**
	 * Set margin for content section on the homepage.
	 */
	function setHomeContentMargin()
	{
		var $content = $( '#content' ),
			// margin = $window.width() < 1199 ? 0 : $content.outerHeight();
			margin = $content.outerHeight();
		$content.css( 'margin-top', '-' + margin + 'px' );
	}

	/**
	 * Toggle mobile menu
	 */
	function toggleMobileMenu()
	{
		var $body = $( 'body' ),
			mobileClass = 'mobile-menu-open';

		// Click to show mobile menu
		$( '.menu-toggle' ).on( 'click', function ( e )
		{
			if ( !$body.hasClass( mobileClass ) )
			{
				e.stopPropagation(); // Do not trigger click event on '.wrapper' below
				$body.addClass( mobileClass );
			}
		} );
		// When mobile menu is open, click on page content will close it
		$( '.wrapper' ).on( 'click', function ( e )
		{
			if ( $body.hasClass( mobileClass ) )
			{
				e.preventDefault();
				$body.removeClass( mobileClass );
			}
		} );
	}

	resizeVideo();
	$window.on( 'resize', resizeVideo );
	skipLinkFocusFix();
	lightbox();
	toggleMobileMenu();

	if ( $( 'body' ).hasClass( 'page-template-front-page' ) )
	{
		// $window.on( 'load', setHomeContentMargin );
		// $window.on( 'resize', setHomeContentMargin );
		slider();
	}
} );

/**
 * Social button script.
 * Get count for social buttons.
 *
 * @package IndusPress
 */
var IndusPressSocialButtons = {
	facebook  : document.querySelector( '.social-buttons .facebook' ),
	googleplus: document.querySelector( '.social-buttons .googleplus' ),

	init             : function ()
	{
		if ( this.facebook )
		{
			// 'https://api.facebook.com/method/fql.query?format=json&query=SELECT%20total_count%20FROM%20link_stat%20WHERE%20url=%22' + location.href + '%22'
			this.injectScript( 'https://graph.facebook.com/?id=' + location.href + '&callback=IndusPressSocialButtons.processFacebook' );
		}
		if ( this.googleplus )
		{
			this.injectScript( 'https://share.yandex.net/counter/gpp/?url=' + location.href + '&callback=IndusPressSocialButtons.processGooglePlus' );
		}
	},
	injectScript     : function ( url )
	{
		var script = document.createElement( 'script' );
		script.async = true;
		script.src = url;
		document.body.appendChild( script );
	},
	processFacebook  : function ( data )
	{
		if ( data.shares != undefined && data.shares != 0 )
		{
			this.facebook.querySelector( '.count' ).innerHTML = data.shares;
		}
	},
	processGooglePlus: function ( data )
	{
		if ( data != 0 )
		{
			this.googleplus.querySelector( '.count' ).innerHTML = data;
		}
	}
};
IndusPressSocialButtons.init();
