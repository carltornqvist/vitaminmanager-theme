/**
 * Script for customizer controls.
 */
(function ( $, api )
{
	// View documentation
	$( '<a href="http://gretathemes.com/docs/" target="_blank"></a>' )
		.text( indusPressCustomizer.docs )
		.css( {
			'display'       : 'inline-block',
			'border-radius' : '2px',
			'background'    : '#ea6f60',
			'color'         : '#fff',
			'text-transform': 'uppercase',
			'margin'        : '6px 10px 0 0',
			'padding'       : '3px 6px',
			'font-size'     : '9px',
			'letter-spacing': '1px',
			'line-height'   : 1.5
		} )
		.appendTo( '.preview-notice' );

	// Radio image control.
	api.controlConstructor['radio-image'] = api.Control.extend( {
		ready: function ()
		{
			var control = this;
			$( 'input:radio', control.container ).change( function ()
			{
				control.setting.set( $( this ).val() );
			} );
		}
	} );
})( jQuery, wp.customize );
