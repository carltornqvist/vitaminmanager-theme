/**
 * Add more clients.
 * @package IndusPress
 * @author GretaThemes
 */
jQuery( document ).ready( function ( $ )
{
	var index = $( '.induspress-client' ).length - 1;

	$( 'body' )
		// Add more client
		.on( 'click', '.induspress-clients-add', function ( e )
		{
			e.preventDefault();
			var $this = $( this ),
				$clone = $this.prev().clone().insertBefore( $this );

			$clone
				// Show 'remove' link
				.find( '.induspress-client-remove' ).removeClass( 'hidden' ).end()
				// Hide image
				.find( 'img' ).addClass( 'hidden' ).attr( 'src', '' ).end()
				// Fix input name
				.find( 'input' ).each( function ()
				{
					var $field = $( this ),
						name = $field.attr( 'name' ).replace( /\[\d*?\]\[([a-z]*?)\]$/, '[' + index + '][$1]' );
					$field.val( '' ).attr( 'name', name );
				} );
			index++;
		} )
		// Remove client
		.on( 'click', '.induspress-client-remove', function ( e )
		{
			e.preventDefault();
			$( this ).closest( '.induspress-client' ).remove();
		} );
} );
