jQuery( function ( $ )
{
	var $body = $( 'body' );

	function toggleRadio()
	{
		$body.on( 'click', '.icons-select label[for]', function ()
		{
			var $this = $( this ),
				$parent = $this.closest( '.icons-select' ),
				labelFor = $this.attr( 'for' ),
				$radio = $parent.find( '#' + labelFor );

			$parent.find( 'label[for]' ).removeClass( 'active' );
			$this.addClass( 'active' );
			$parent.find( 'input[type=radio]' ).prop( 'checked', false );
			$radio.prop( 'checked', true );
		} );
	}

	toggleRadio();
} );

