<?php
/**
 * The template part that shows hero content for the front page.
 * @package IndusPress
 */
?>

<div class="hero owl-carousel" id="hero-slider">
	<?php
	$child_pages = get_posts( array(
		'post_type'      => 'page',
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_parent'    => get_the_ID(),
		'posts_per_page' => - 1,
		'no_found_rows'  => true,
		'post_status'    => 'any', // Can use draft pages to create slider
	) );
	// No child pages: show hero content of front page
	$child_pages = empty( $child_pages ) ? array( get_post() ) : $child_pages;
	?>
	<?php foreach ( $child_pages as $child_page ) : ?>
		<?php
		$thumbnail_id = get_post_thumbnail_id( $child_page->ID );
		list( $image ) = wp_get_attachment_image_src( $thumbnail_id, 'full' );
		$page_content = get_post_field( 'post_content', $child_page );
		$page_content = get_extended( $page_content );
		?>
		<div class="slide">
			<img src="<?php echo esc_url( $image ); ?>" alt="slide">
			<div class="container slide__content">
				<?php induspress_content( $page_content['main'] ); ?>
			</div>
		</div>
	<?php endforeach; ?>
</div>
