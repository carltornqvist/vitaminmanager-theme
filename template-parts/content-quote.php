<?php
/**
 * The template for displaying quote post formats
 * Used for index/archive/search.
 *
 * @package IndusPress
 */
?>

<div class="entry-content clearfix">
	<?php the_content(); ?>
</div>
