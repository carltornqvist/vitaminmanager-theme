<?php
/**
 * The template part that shows hero content.
 * @package IndusPress
 */

global $post;

$custom_temp = $post->post_type == 'vitamin' || $post->post_type == 'vitamin_packs';

if ( !$custom_temp && $hero_content = induspress_setting( 'hero_content' ) ) : ?>

	<div class="hero header entry-header">
		<div class="container">
			<?php induspress_content( $hero_content ); ?>
		</div>
	</div>

<?php endif; ?>