<?php
/**
 * The template part that shows header top text.
 * @package IndusPress
 */
?>

<?php if ( $text = induspress_setting( 'header_top_text' ) ) : ?>
	<div class="header-top">
		<div class="container">
			<?php echo do_shortcode( $text ); ?>
		</div>
	</div>
<?php endif; ?>
