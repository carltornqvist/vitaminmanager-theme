<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
	<?php __( 'Pingback:', 'induspress' ); ?><?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'induspress' ), '<span class="edit-link">', '</span>' ); ?>
</li>
