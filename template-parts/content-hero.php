<?php induspress_breadcrumbs(); ?>
<h1 class="entry-title"><?php the_title(); ?></h1>
<?php
/**
 * Checks to see if the author has a Gravatar image.
 * @link https://tommcfarlin.com/check-if-a-user-has-a-gravatar/
 */
$author_email = get_the_author_meta( 'user_email' );
$url          = 'http://www.gravatar.com/avatar/' . md5( strtolower( trim( $author_email ) ) ) . '?d=404';
$headers      = @get_headers( $url );

// Only show author Gravatar if available
$has_avatar = preg_match( '|200|', $headers[0] );
?>
<p class="entry-meta<?php echo $has_avatar ? '' : ' no-avatar'; ?>">
	<?php
	echo $has_avatar ? get_avatar( $author_email, 32 ) : '';
	printf(
		__( 'by %s &mdash; on %s.', 'induspress' ),
		induspress_entry_meta_element( 'author' ),
		induspress_entry_meta_element( 'published_date' )
	);
	comments_popup_link(
		__( 'No comment.', 'induspress' ),
		__( '1 Comment.', 'induspress' ),
		__( '% Comments.', 'induspress' ),
		'comments-link',
		__( 'Comments are closed.', 'induspress' )
	);
	?>
	</p>
