<h2 class="site-title"><?php _e( 'Error 404', 'induspress' ); ?></h2>
<h3 class="site-description">
	<?php _e( "It seems like you have tried to open a page that doesn't exist. It could have been deleted, moved, or it never existed at all. You are welcome to search for what you are looking for with the form below.", 'induspress' ) ?>
</h3>
<?php get_search_form(); ?>
