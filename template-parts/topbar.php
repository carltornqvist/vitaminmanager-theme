<?php
/**
 * The template for displaying topbar.
 *
 * @package IndusPress
 */

if ( is_active_sidebar( 'topbar-left' ) || is_active_sidebar( 'topbar-right' ) ) : ?>
	<div class="topbar hidden-xs">
		<div class="container clearfix">
			<?php if ( is_active_sidebar( 'topbar-left' ) ) : ?>
				<div class="topbar-widgets left">
					<?php dynamic_sidebar( 'topbar-left' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'topbar-right' ) ) : ?>
				<div class="topbar-widgets right">
					<?php dynamic_sidebar( 'topbar-right' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>