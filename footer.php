<?php
/**
 * The template part for displaying footer section.
 * @package IndusPress
 */
?>

</main>

<footer class="footer" role="contentinfo">

	<?php if ( is_active_sidebar( 'footer' ) ) : ?>
		<div class="footer-widgets clearfix">
			<div class="container">
				<div class="grid">
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="credits clearfix">
		<div class="container">
			<div class="left">
				<?php
				if ( $footer_left = induspress_setting( 'footer_left' ) )
				{
					echo do_shortcode( $footer_left );
				}
				else
				{
					printf(
						esc_html__( 'Copyright &copy; %s %s.', 'induspress' ),
						date_i18n( 'Y' ),
						sprintf( '<a href="%s">%s</a>', esc_url( home_url() ), esc_html( get_bloginfo() ) )
					);
				}
				?>
			</div>
			<div class="right">
				<?php
				if ( $footer_right = induspress_setting( 'footer_right' ) )
				{
					echo do_shortcode( $footer_right );
				}
				else
				{
					printf(
						esc_html__( 'Theme %s by %s. Powered by %s.', 'induspress' ),
						sprintf( '<a href="%s">%s</a>', esc_url( 'http://gretathemes.com/wordpress-themes/induspress/' ), 'IndusPress' ),
						'GretaThemes',
						sprintf( '<a href="%s">%s</a>', esc_url( 'http://wordpress.org/' ), 'WordPress' )
					);
				}
				?>
			</div>
		</div>
	</div>
</footer><!-- .footer -->

</div><!-- .wrapper -->

<?php wp_footer(); ?>
<nav class="mobile-navigation" role="navigation">
	<?php
	wp_nav_menu( array(
		'container_class' => 'mobile-menu',
		'menu_class'      => 'mobile-menu clearfix',
		'theme_location'  => 'primary',
		'items_wrap'      => '<ul>%3$s</ul>',
	) );
	?>
</nav>
</body>
</html>
