<?php
/**
 * Theme functions file which contains global functions of the theme
 * @package IndusPress
 */

// Load theme files
require get_template_directory() . '/inc/media.php';
require get_template_directory() . '/inc/header.php';
require get_template_directory() . '/inc/customizer/customizer.php';
new IndusPress_Customizer;
require get_template_directory() . '/inc/color-scheme.php';
new IndusPress_Color_Scheme;
require get_template_directory() . '/inc/layout.php';
new IndusPress_Layout;

// Load file for the frontend only
if ( ! is_admin() )
{
	require get_template_directory() . '/inc/template-tags.php';
	require get_template_directory() . '/inc/cleaner-gallery.php';
	require get_template_directory() . '/inc/breadcrumbs.php';
}

add_action( 'after_setup_theme', 'induspress_setup' );

/**
 * Setup theme
 */
function induspress_setup()
{
	// Theme features
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-formats', array( 'aside', 'quote', 'video', 'audio' ) );
	add_theme_support( 'custom-header', array(
		'width'         => 1920,
		'height'        => 500,
		'uploads'       => true,
		'default-image' => get_template_directory_uri() . '/img/header.jpg',
		'header-text'   => false,
	) );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'induspress-list-thumbnail', 760, 0 );
	add_image_size( 'induspress-grid-thumbnail', 348, 0 );

	// Custom theme features
	add_theme_support( 'induspress-social-buttons' );

	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'induspress' ),
	) );

	// Make the theme translation ready
	load_theme_textdomain( 'induspress', get_template_directory() . '/lang' );
}

add_action( 'widgets_init', 'induspress_register_sidebars' );

/**
 * Register theme sidebars.
 */
function induspress_register_sidebars()
{
	register_sidebar( array(
		'name'          => __( 'Primary', 'induspress' ),
		'id'            => 'primary',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Homepage', 'induspress' ),
		'id'            => 'home',
		'before_widget' => '<section class="section %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Topbar left', 'induspress' ),
		'id'            => 'topbar-left',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Topbar right', 'induspress' ),
		'id'            => 'topbar-right',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Header', 'induspress' ),
		'id'            => 'header',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title">',
		'after_title'   => '</div>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer', 'induspress' ),
		'id'            => 'footer',
		'before_widget' => '<div id="%1$s" class="widget column one-fourth %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Download', 'induspress' ),
		'id'            => 'download',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title accordion-section-title"><i class="icon icon-folder"></i>',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'induspress_register_widgets' );

/**
 * Register theme custom widgets.
 */
function induspress_register_widgets()
{
	require get_template_directory() . '/inc/widgets/dribbble.php';
	require get_template_directory() . '/inc/widgets/flickr.php';
	require get_template_directory() . '/inc/widgets/posts.php';
	require get_template_directory() . '/inc/widgets/services.php';
	require get_template_directory() . '/inc/widgets/welcome.php';
	require get_template_directory() . '/inc/widgets/products.php';
	require get_template_directory() . '/inc/widgets/call-to-action.php';
	require get_template_directory() . '/inc/widgets/news.php';
	require get_template_directory() . '/inc/widgets/projects.php';
	require get_template_directory() . '/inc/widgets/clients.php';
	require get_template_directory() . '/inc/widgets/contact-info.php';
	require get_template_directory() . '/inc/widgets/downloads.php';

	register_widget( 'IndusPress_Widget_Flickr' );
	register_widget( 'IndusPress_Widget_Dribbble' );
	register_widget( 'IndusPress_Widget_Posts' );
	register_widget( 'IndusPress_Widget_Services' );
	register_widget( 'IndusPress_Widget_Welcome' );
	register_widget( 'IndusPress_Widget_Products' );
	register_widget( 'IndusPress_Widget_Call_To_Action' );
	register_widget( 'IndusPress_Widget_News' );
	register_widget( 'IndusPress_Widget_Projects' );
	register_widget( 'IndusPress_Widget_Clients' );
	register_widget( 'IndusPress_Widget_Contact_Info' );
	register_widget( 'IndusPress_Widget_Downloads' );
}

// Set content-width
if ( ! isset( $content_width ) )
{
	$content_width = 760;
}

/**
 * Get theme default settings.
 * @param string $name
 * @return mixed
 */
function induspress_default_setting( $name )
{
	$defaults = array(
		'archive_layout_style'   => 'thumb-text',
		'archive_sidebar_layout' => 'right',
		'single_sidebar_layout'  => 'right',
		'page_sidebar_layout'    => 'right',
	);
	return isset( $defaults[$name] ) ? $defaults[$name] : false;
}

/**
 * Get theme settings.
 * @param string $name
 * @return mixed
 */
function induspress_setting( $name )
{
	return get_theme_mod( $name, induspress_default_setting( $name ) );
}

/**
CUSTOM VM FIXES
*/
function vm_jetpackcom_custom_required() {
   return '('.__( 'required', 'induspress' ).')';
}
add_filter( 'jetpack_required_field_text', 'vm_jetpackcom_custom_required' );

// sss remove the subscription column in users table
remove_filter( 'manage_users_columns', 'WC_Subscriptions_Admin::add_user_columns', 11 );
remove_filter( 'manage_users_custom_column', 'WC_Subscriptions_Admin::user_column_values', 11 );

if ( is_admin() ) {
    add_filter( 'wp_count_comments', function( $counts, $post_id ) {
        if ( $post_id )
            return $counts;
        return (object) array(
            'approved'        => 0,
            'spam'            => 0,
            'trash'           => 0,
            'total_comments'  => 0,
            'moderated'       => 0,
            'post-trashed'    => 0,
        );
    }, 10, 2 );
}

// If running WooCommerce, remove their filter so that nothing funky goes down
remove_filter( 'wp_count_comments', array( 'WC_Comments', 'wp_count_comments' ), 10 );

// Remove order count from admin menu
add_filter( 'woocommerce_include_processing_order_count_in_menu', '__return_false' );
//
function my_loginlogo() {
  echo '<style type="text/css">
    h1 a {
      background-image: url(/wp-content/uploads/2016/04/vmlogo2.png) !important;
    }
  </style>';
}
add_action('login_head', 'my_loginlogo');

function my_loginURL() {
    return 'https://www.vitaminmanager.com';
}
add_filter('login_headerurl', 'my_loginURL');

function my_loginURLtext() {
    return 'Vitamin Manager';
}
add_filter('login_headertitle', 'my_loginURLtext');

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login_styles.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');


/*********************************/
/**** Email to a new customer ****/
/*_______________________________*/

remove_action('register_new_user', 'wp_send_new_user_notifications');

function wp_user_notification_and_order( $user_id, $order = null, $deprecated = null, $notify = 'both') {
	if ( $deprecated !== null ) {
		_deprecated_argument( __FUNCTION__, '4.3.1' );
	}

	global $wpdb, $wp_hasher;
	$user = get_userdata( $user_id );

	// The blogname option is escaped with esc_html on the way into the database in sanitize_option
	// we want to reverse this for the plain text arena of emails.
	$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
	$message .= sprintf(__('Email: %s'), $user->user_email) . "\r\n";

	@wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

	// `$deprecated was pre-4.3 `$plaintext_pass`. An empty `$plaintext_pass` didn't sent a user notifcation.
	if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
		return;
	}

	// Generate something random for a password reset key.
	$key = wp_generate_password( 20, false );

	/** This action is documented in wp-login.php */
	do_action( 'retrieve_password_key', $user->user_login, $key );

	// Now insert the key, hashed, into the DB.
	if ( empty( $wp_hasher ) ) {
		require_once ABSPATH . WPINC . '/class-phpass.php';
		$wp_hasher = new PasswordHash( 8, true );
	}
	$hashed = time() . ':' . $wp_hasher->HashPassword( $key );
	$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

	$resetHref = network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login');
	$message = sprintf(__('Username: %s'), $user->user_login) . "<br><br>";
	$message .= __('To set your password, visit the following address:') . "<br>";
	$message .= '<a href="' . $resetHref . '">' . $resetHref . '</a><br><br>';

	// $message .= wp_login_url() . "\r\n";

	if(!empty($order)){
		$message .= my_custom_table($order);
	}

  $message = str_replace(array("\r","\n","\t"),"",$message);
	$message .= '';
	$headers = array('Content-Type: text/html; charset=UTF-8');

	wp_mail($user->user_email, sprintf(__('[%s] Your username and password info'), $blogname), default_email_html_wrapper($message), $headers);
}

function my_custom_table($order){
    ob_start(); ?>
    <!-- <div></div> -->
    <h3>DIN PRENUMERATION</h3>
    <table class="td" cellspacing="0" cellpadding="6" style="width: 100%;border: 1px solid #eee; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="0">
        <thead>
        <tr>
            <th class="td" scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
            <th class="td" scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
            <th class="td" scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Price', 'woocommerce' ); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php echo $order->email_order_items_table( array(
            'show_sku'      => true,
            'show_image'    => true,
            'image_size'    => array( 32, 32 ),
            'plain_text'    => false,
            'sent_to_admin' => false
        ) ); ?>
        </tbody>
        <tfoot>
        <?php
        if ( $totals = $order->get_order_item_totals() ) {
            $i = 0;
            foreach ( $totals as $total ) {
                $i++;
                ?>	<tr>
                <th class="td" scope="row" colspan="2" style="border: 1px solid #eee; text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 1px;'; ?>"><?php echo $total['label']; ?></th>
                <td class="td" style="border: 1px solid #eee; text-align:left; <?php if ( $i === 1 ) echo 'border-top-width: 1px;'; ?>"><?php echo $total['value']; ?></td>
                </tr><?php
            }
        }
        ?>
        </tfoot>
    </table>
   	<!-- <div></div> -->


    <?php $order_description = ob_get_contents();
    ob_end_clean();

    return $order_description;
}

function default_email_html_wrapper($content) {
  ob_start(); ?>
  <div style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
      <tbody>
        <tr>
          <td style="padding: 15px;"><center>
            <table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
              <tbody>
                <tr>
                  <td align="left">
                    <div style="border: solid 1px #d9d9d9;">
                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
                          </tr>
                          <tr>
                            <td style="line-height: 32px; padding-left: 30px; text-align: center;" colspan="2" valign="baseline"><a href="http://vitaminmanager.com/wp-content/uploads/2015/05/VitaminManager_Big.jpg"><img class="size-medium wp-image-2965 aligncenter" src="http://vitaminmanager.com/wp-content/uploads/2015/05/VitaminManager_Big-300x172.jpg" alt="Vitamin Manager Logotype" width="300" height="172" /></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="content" style="margin-top: 15px; margin-right: 30px; margin-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="490" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div style="padding: 15px 0;">
                                <div style="padding: 15px 0;">
                                  	<?php echo $content; ?>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="footer" style="line-height: 1.5; font-size: 12px; font-family: Arial, sans-serif; margin-right: 30px; margin-left: 30px;" border="0" width="490" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr style="font-size: 11px; color: #999999;">
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div><img style="vertical-align: middle;" src="https://vitaminmanager.com/wp-admin/images/comment-grey-bubble.png" alt="Kontakta" width="12" height="12" /> För frågor och funderingar, vänligen kontakta <a href="mailto:kundservice@vitaminmanager.com">kundservice@vitaminmanager.com</a></div>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" height="15">.</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </center></td>
        </tr>
      </tbody>
    </table>
  </div>
  <?php $html = ob_get_contents();
    ob_end_clean();

    return $html;
}
