<?php
/**
 * Created by PhpStorm.
 * User: Denys
 * Date: 24.01.2016
 * Time: 19:17
 */

global $wpo_wcpdf;
?>
<table class="head container">
    <tr>
        <td class="header">
            <?php
            if( $wpo_wcpdf->get_header_logo_id() ) {
                $wpo_wcpdf->header_logo();
            } else {
                echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Invoice', 'wpo_wcpdf' ) );
            }
            ?>
        </td>
        <td class="shop-info">
            <div class="shop-name"><h3><?php $wpo_wcpdf->shop_name(); ?></h3></div>
            <div class="shop-address"><?php $wpo_wcpdf->shop_address(); ?></div>
        </td>
    </tr>
</table>

<h1 class="document-type-label">
    <?php if( $wpo_wcpdf->get_header_logo_id() ) echo apply_filters( 'wpo_wcpdf_invoice_title', __( 'Invoice', 'wpo_wcpdf' ) ); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order ); ?>

<table class="order-data-addresses">
    <tr>
        <td class="address billing-address">
            <!-- <h3><?php _e( 'Billing Address:', 'wpo_wcpdf' ); ?></h3> -->
            <?php $wpo_wcpdf->billing_address(); ?>
            <?php if ( isset($wpo_wcpdf->settings->template_settings['invoice_email']) ) { ?>
                <div class="billing-email"><?php $wpo_wcpdf->billing_email(); ?></div>
            <?php } ?>
            <?php if ( isset($wpo_wcpdf->settings->template_settings['invoice_phone']) ) { ?>
                <div class="billing-phone"><?php $wpo_wcpdf->billing_phone(); ?></div>
            <?php } ?>
        </td>
        <td class="address shipping-address">
            <?php if ( isset($wpo_wcpdf->settings->template_settings['invoice_shipping_address']) && $wpo_wcpdf->ships_to_different_address()) { ?>
                <h3><?php _e( 'Ship To:', 'wpo_wcpdf' ); ?></h3>
                <?php $wpo_wcpdf->shipping_address(); ?>
            <?php } ?>
        </td>
        <td class="order-data">
            <table>
                <?php do_action( 'wpo_wcpdf_before_order_data', $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order ); ?>
                <?php if ( isset($wpo_wcpdf->settings->template_settings['display_number']) && $wpo_wcpdf->settings->template_settings['display_number'] == 'invoice_number') { ?>
                    <tr class="invoice-number">
                        <th><?php _e( 'Invoice Number:', 'wpo_wcpdf' ); ?></th>
                        <td><?php $wpo_wcpdf->invoice_number(); ?></td>
                    </tr>
                <?php } ?>
                <?php if ( isset($wpo_wcpdf->settings->template_settings['display_date']) && $wpo_wcpdf->settings->template_settings['display_date'] == 'invoice_date') { ?>
                    <tr class="invoice-date">
                        <th><?php _e( 'Invoice Date:', 'wpo_wcpdf' ); ?></th>
                        <td><?php $wpo_wcpdf->invoice_date(); ?></td>
                    </tr>
                <?php } ?>

                <tr class="order-date">
                    <th><?php _e( 'Order Date:', 'wpo_wcpdf' ); ?></th>
                    <td><?php $wpo_wcpdf->order_date(); ?></td>
                </tr>
                <tr class="payment-method">
                    <th><?php _e( 'Payment Method:', 'wpo_wcpdf' ); ?></th>
                    <td><?php $wpo_wcpdf->payment_method(); ?></td>
                </tr>
                <?php do_action( 'wpo_wcpdf_after_order_data', $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order ); ?>
            </table>
        </td>
    </tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order ); ?>

<table class="order-details">
    <thead>
    <tr>
        <th class="product-image"><?php _e('Utseende', 'wpo_wcpdf'); ?></th>
        <th class="product"><?php _e('Product', 'wpo_wcpdf'); ?></th>
        <th class="quantity"><?php _e('Quantity', 'wpo_wcpdf'); ?></th>
        <th class="price"><?php _e('Price', 'wpo_wcpdf'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $counter = 0; $items = $wpo_wcpdf->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
        <tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order, $item_id ); ?>">
            <td class="product-image">
                <?php
                $product = wc_get_product($item['product_id']);
                echo $product->get_image();
                ?>

            </td>
            <td class="product">
                <?php $description_label = __( 'Description', 'wpo_wcpdf' ); // registering alternate label translation ?>
                <span class="item-name"><?php echo $item['name']; ?></span>
                <?php do_action( 'wpo_wcpdf_before_item_meta', $wpo_wcpdf->export->template_type, $item, $wpo_wcpdf->export->order  ); ?>
                <span class="item-meta"><?php echo $item['meta']; ?></span>
                <dl class="meta">
                    <?php $description_label = __( 'SKU', 'wpo_wcpdf' ); // registering alternate label translation ?>
                    <?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'wpo_wcpdf' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
                    <?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'wpo_wcpdf' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
                </dl>
                <?php do_action( 'wpo_wcpdf_after_item_meta', $wpo_wcpdf->export->template_type, $item, $wpo_wcpdf->export->order  ); ?>
            </td>
            <td class="quantity"><?php echo $item['quantity']; ?></td>

            <td class="price">
                <?php

                $price_check = strlen(str_replace('0,00', '', $item['order_price']));
                if($price_check === 40) {
                    echo '-';
                } else {
                    echo $item['order_price'];
                }
                ?>
            </td>
        </tr>
    <?php endforeach; endif; ?>
    </tbody>
    <tfoot>
    <tr class="no-borders">
        <td style="border: none;"></td>
        <td class="no-borders">
            <div class="customer-notes">
                <?php if ( $wpo_wcpdf->get_shipping_notes() ) : ?>
                    <h3><?php _e( 'Customer Notes', 'wpo_wcpdf' ); ?></h3>
                    <?php $wpo_wcpdf->shipping_notes(); ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="no-borders" colspan="2">
            <table class="totals">
                <tfoot>
                <?php foreach( $wpo_wcpdf->get_woocommerce_totals() as $key => $total ) : ?>
                    <tr class="<?php echo $key; ?>">
                        <td class="no-borders"></td>
                        <th class="description"><?php echo $total['label']; ?></th>
                        <td class="price"><span class="totals-price"><?php echo str_replace('via Flat Rate', '', $total['value']); ?></span></td>
                    </tr>
                <?php endforeach; ?>
                </tfoot>
            </table>
        </td>
    </tr>
    </tfoot>
</table>
<div id="bottom-info">

    <?php do_action( 'wpo_wcpdf_after_order_details', $wpo_wcpdf->export->template_type, $wpo_wcpdf->export->order ); ?>
    <div id="additional-info">
        <?php
        /**
         * Hacky way to deal with strtotime not working in different locales
         * TODO: find a better solution (strptime, strftime)
         */
        //Call method to be sure date on order is set
        $fake_date = $wpo_wcpdf->get_invoice_date();

        $real_date = get_post_meta($wpo_wcpdf->export->order->id,'_wcpdf_invoice_date',true);

        ?>
        Betala till bankgiro 604-3962 senast <?php echo date_i18n( get_option( 'date_format' ), strtotime("+30 days", strtotime($real_date))); ?>. Ange referens: <?php $wpo_wcpdf->invoice_number(); ?>.
    </div><!-- #additional-info -->
    <?php if ( $wpo_wcpdf->get_footer() ): ?>
        <div id="footer">
            <?php $wpo_wcpdf->footer(); ?>
        </div><!-- #letter-footer -->
    <?php endif; ?>
</div><!-- #bottom-info -->
