<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div style="margin: 0px; background-color: #F4F3F4; font-family: Helvetica, Arial, sans-serif; font-size:12px;" text="#444444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
      <tbody>
        <tr>
          <td style="padding: 15px;"><center>
            <table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
              <tbody>
                <tr>
                  <td align="left">
                    <div style="border: solid 1px #d9d9d9;">
                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
                          </tr>
                          <tr>
                            <td style="line-height: 32px; padding-left: 30px; text-align: center;" colspan="2" valign="baseline"><a href="http://vitaminmanager.com/wp-content/uploads/2015/05/VitaminManager_Big.jpg"><img class="size-medium wp-image-2965 aligncenter" src="http://vitaminmanager.com/wp-content/uploads/2015/05/VitaminManager_Big-300x172.jpg" alt="Vitamin Manager Logotype" width="300" height="172" /></a></td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="content" style="margin-top: 15px; margin-right: 30px; margin-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" border="0" width="490" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div style="padding: 15px 0;">
                                <div style="padding: 15px 0;">
                                  	<p><?php _e( 'Someone requested that the password be reset for the following account:', 'woocommerce' ); ?></p>
									<p><?php printf( __( 'Username: %s', 'woocommerce' ), $user_login ); ?></p>
									<p><?php _e( 'If this was a mistake, just ignore this email and nothing will happen.', 'woocommerce' ); ?></p>
									<p><?php _e( 'To reset your password, visit the following address:', 'woocommerce' ); ?></p>
									<p>
										<a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>">
												<?php _e( 'Click here to reset your password', 'woocommerce' ); ?></a>
									</p>
									<p></p>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="footer" style="line-height: 1.5; font-size: 12px; font-family: Arial, sans-serif; margin-right: 30px; margin-left: 30px;" border="0" width="490" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr style="font-size: 11px; color: #999999;">
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div><img style="vertical-align: middle;" src="https://vitaminmanager.com/wp-admin/images/comment-grey-bubble.png" alt="Kontakta" width="12" height="12" /> För frågor och funderingar, vänligen kontakta <a href="mailto:kundservice@vitaminmanager.com">kundservice@vitaminmanager.com</a></div>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" height="15">.</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </center></td>
        </tr>
      </tbody>
    </table>
  </div>